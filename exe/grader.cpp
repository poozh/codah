#include "Grader.h"
#include "grader_constants.h"
#include "grader/grader.hpp"
#include "grader/grader_impl.hpp"

#include <gflags/gflags.h>
#include <glog/logging.h>
#include <iostream>
#include <thrift/concurrency/ThreadFactory.h>
#include <thrift/concurrency/ThreadManager.h>
#include <thrift/protocol/TBinaryProtocol.h>
#include <thrift/server/TThreadPoolServer.h>
#include <thrift/transport/TBufferTransports.h>
#include <thrift/transport/TServerSocket.h>

// #include <zi/system/daemon.hpp>

DEFINE_int32(port, 9876, "Server's port");
// DEFINE_bool(daemonize, false, "Run as daemon");
DEFINE_string(tasks_root, "./tasks", "Root dir where tasks are located");
DEFINE_int32(num_boxes, 2, "Number of parallel sandboxes");
DEFINE_string(isolate_path, "/usr/local/bin/isolate",
              "Location of the isolate binary");
DEFINE_string(sandboxes_root, "/tmp/box", "Sandboxes root directory");
DEFINE_bool(recompile_checkers, false,
            "Should re-compile all the available checkers?");

using namespace ::apache::thrift;
using namespace ::apache::thrift::protocol;
using namespace ::apache::thrift::transport;
using namespace ::apache::thrift::server;

using apache::thrift::concurrency::ThreadFactory;
using apache::thrift::concurrency::ThreadManager;

using std::shared_ptr;

using namespace ::codah::services;

#define THROW_GENERAL_ERROR(what)                                              \
    {                                                                          \
        GraderException ge;                                                    \
        ge.error_code = g_grader_constants.GRADER_GENERAL_ERROR;               \
        ge.message    = what;                                                  \
        throw ge;                                                              \
    }                                                                          \
    static_cast<void>(0)

#define THROW_GENERAL_ERROR_IF(condition, what)                                \
    if ((condition))                                                           \
    {                                                                          \
        GraderException ge;                                                    \
        ge.error_code = g_grader_constants.GRADER_GENERAL_ERROR;               \
        ge.message    = what;                                                  \
        throw ge;                                                              \
    }                                                                          \
    static_cast<void>(0)

class GraderHandler : virtual public GraderIf
{
private:
    std::unique_ptr<codah::grader_impl> grader_;

    void fillGradeReport(GradeReport& _return, codah::grade_report const& r)
    {
        _return.status = r.status;

        if (r.status == g_grader_constants.STATUS_OK)
        {
            _return.test_cases.resize(r.test_cases.size());
            for (std::size_t i = 0; i < r.test_cases.size(); ++i)
            {
                _return.test_cases[i].status    = r.test_cases[i].status;
                _return.test_cases[i].time      = r.test_cases[i].time;
                _return.test_cases[i].wall_time = r.test_cases[i].wall_time;
                _return.test_cases[i].memory    = r.test_cases[i].memory;
                _return.test_cases[i].solved    = r.test_cases[i].solved;
                _return.test_cases[i].test_num  = r.test_cases[i].test_num;
            }
        }
        else
        {
            _return.status_message = r.status_message;
        }
    }

    void verifySource(const std::string& source, const int32_t language)
    {
        THROW_GENERAL_ERROR_IF(!codah::compiler_traits::is_supported(language),
                               "Language not supported as checker");

        THROW_GENERAL_ERROR_IF(source.size() > 65536, "Source file too big");
    }

public:
    GraderHandler()
        : grader_(std::make_unique<codah::grader_impl>(
              FLAGS_tasks_root, FLAGS_num_boxes, FLAGS_isolate_path,
              FLAGS_recompile_checkers))
    {
        // Your initialization goes here
    }

    void fullGrade(GradeReport& _return, const std::string& source,
                   const std::string& task, const int32_t language,
                   const int32_t time_limit,
                   const int32_t memory_limit) override
    {
        verifySource(source, language);

        using codah::compiler_type;

        compiler_type type = static_cast<compiler_type>(language);

        auto r =
            grader_->full_grade(task, source, type, time_limit, memory_limit);

        THROW_GENERAL_ERROR_IF(r.first, *r.first);

        fillGradeReport(_return, r.second);
    }

    void partialGrade(GradeReport& _return, const std::string& source,
                      const std::string& task, const int32_t language,
                      const int32_t time_limit, const int32_t memory_limit,
                      const int32_t first_test,
                      const int32_t last_test) override
    {
        verifySource(source, language);

        using codah::compiler_type;

        compiler_type type = static_cast<compiler_type>(language);

        auto r = grader_->partial_grade(task, source, type, time_limit,
                                        memory_limit, first_test, last_test);

        THROW_GENERAL_ERROR_IF(r.first, *r.first);

        fillGradeReport(_return, r.second);
    }

    void getTestInput(std::string& _return, const std::string& task,
                      const int32_t nCase) override
    {
        auto r = grader_->get_test_input(task, nCase);
        if (r.first)
        {
            _return = std::move(r.second);
        }
        else
        {
            GraderException ge;
            ge.error_code = g_grader_constants.GRADER_GENERAL_ERROR;
            ge.message    = std::move(r.second);
            throw ge;
        }
    }

    void getTestSolution(std::string& _return, const std::string& task,
                         const int32_t nCase) override
    {
        auto r = grader_->get_test_solution(task, nCase);
        if (r.first)
        {
            _return = std::move(r.second);
        }
        else
        {
            GraderException ge;
            ge.error_code = g_grader_constants.GRADER_GENERAL_ERROR;
            ge.message    = std::move(r.second);
            throw ge;
        }
    }

    void getTaskState(TaskState& _return, const std::string& task) override
    {
        auto r = grader_->get_task_state(task);

        THROW_GENERAL_ERROR_IF(r.first, *r.first);

        _return.name                = std::move(r.second.name);
        _return.is_locked           = std::move(r.second.is_locked);
        _return.grader_lang         = r.second.grader_lang;
        _return.has_compiled_grader = r.second.has_compiled_grader;

        for (auto const& tc : r.second.test_cases)
        {
            TestCaseState tcs;
            tcs.input_size    = tc.input_size;
            tcs.solution_size = tc.solution_size;
            _return.test_cases.push_back(tcs);
        }
    }

    void getChecker(CodeFile& _return, const std::string& task) override
    {
        auto r = grader_->get_checker(task);

        THROW_GENERAL_ERROR_IF(std::get<0>(r), *std::get<0>(r));
        _return.lang = static_cast<int32_t>(std::get<1>(r));
        _return.code = std::get<2>(r);
    }

    void setChecker(const std::string& task, const int32_t lang,
                    const std::string& code) override
    {
        verifySource(code, lang);

        using codah::compiler_type;

        auto type = static_cast<compiler_type>(lang);

        auto r = grader_->set_checker(task, code, type);

        THROW_GENERAL_ERROR_IF(r, *r);
    }

    void createTask(const std::string& task) override
    {
        if (auto r = grader_->create_task(task); r)
        {
            GraderException ge;
            ge.error_code = g_grader_constants.GRADER_GENERAL_ERROR;
            ge.message    = std::move(*r);
            throw ge;
        }
    }

    void addTestCase(const std::string& task, const std::string& input_file,
                     const std::string& solution_file) override
    {
        if (auto r = grader_->add_test(task, input_file, solution_file); r)
        {
            GraderException ge;
            ge.error_code = g_grader_constants.GRADER_GENERAL_ERROR;
            ge.message    = std::move(*r);
            throw ge;
        }
    }

    void removeTestCase(const std::string& task,
                        const int32_t      test_number) override
    {
        if (auto r = grader_->remove_test(task, test_number); r)
        {
            GraderException ge;
            ge.error_code = g_grader_constants.GRADER_GENERAL_ERROR;
            ge.message    = std::move(*r);
            throw ge;
        }
    }

    void removeAllTestCases(const std::string& task) override
    {
        if (auto r = grader_->remove_all_tests(task); r)
        {
            GraderException ge;
            ge.error_code = g_grader_constants.GRADER_GENERAL_ERROR;
            ge.message    = std::move(*r);
            throw ge;
        }
    }

    void insertTestCase(const std::string& task, const int32_t test_number,
                        const std::string& input_file,
                        const std::string& solution_file) override
    {
        if (auto r = grader_->insert_test(task, test_number, input_file,
                                          solution_file);
            r)
        {
            GraderException ge;
            ge.error_code = g_grader_constants.GRADER_GENERAL_ERROR;
            ge.message    = std::move(*r);
            throw ge;
        }
    }

    void lockTask(const std::string& task) override
    {
        if (auto r = grader_->lock_task(task); r)
        {
            GraderException ge;
            ge.error_code = g_grader_constants.GRADER_GENERAL_ERROR;
            ge.message    = std::move(*r);
            throw ge;
        }
    }

    void unlockTask(const std::string& task) override
    {
        if (auto r = grader_->unlock_task(task); r)
        {
            GraderException ge;
            ge.error_code = g_grader_constants.GRADER_GENERAL_ERROR;
            ge.message    = std::move(*r);
            throw ge;
        }
    }
};

namespace
{
std::function<void(int)> shutdown_handler;

void signal_handler(int signal) { shutdown_handler(signal); }

} // namespace

int main(int argc, char** argv)
{
    google::ParseCommandLineFlags(&argc, &argv, true);
    google::InitGoogleLogging(argv[0]);

    // if (FLAGS_daemonize)
    // {
    //     if (!::zi::system::daemonize(true, true))
    //     {
    //         std::cerr << "Error trying to daemonize." << std::endl;
    //         return -1;
    //     }
    // }

    int                           port = FLAGS_port;
    shared_ptr<GraderHandler>     handler(new GraderHandler());
    shared_ptr<TProcessor>        processor(new GraderProcessor(handler));
    shared_ptr<TServerTransport>  serverTransport(new TServerSocket(port));
    shared_ptr<TTransportFactory> transportFactory(
        new TBufferedTransportFactory());
    shared_ptr<TProtocolFactory> protocolFactory(new TBinaryProtocolFactory());

    shared_ptr<ThreadManager> threadManager(
        ThreadManager::newSimpleThreadManager(16));
    shared_ptr<ThreadFactory> threadFactory(new ThreadFactory());

    threadManager->threadFactory(threadFactory);
    threadManager->start();

    shared_ptr<TServer> server(
        new TThreadPoolServer(processor, serverTransport, transportFactory,
                              protocolFactory, threadManager));

    shutdown_handler = [&](int s) {
        std::cout << "RECEIVED SIGNAL: " << s << " ... SHUTTING DOWN"
                  << std::endl;
        server->stop();
    };

    signal(SIGTERM, signal_handler);
    signal(SIGABRT, signal_handler);
    signal(SIGINT, signal_handler);

    LOG(INFO) << "Server Ready";

    server->serve();

    std::cout << "DONE\n";

    return 0;
}

#undef THROW_GENERAL_ERROR
#undef THROW_GENERAL_ERROR_IF
