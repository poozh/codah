<?php

define('THRIFT_PHP_LIB', __DIR__ . '/assets');
define('GEN_PHP_DIR', __DIR__ . '/assets');

require_once THRIFT_PHP_LIB . '/Thrift/ClassLoader/ThriftClassLoader.php';

$loader = new Thrift\ClassLoader\ThriftClassLoader();
$loader->registerNamespace('Thrift', THRIFT_PHP_LIB);
$loader->registerNamespace('Codah', GEN_PHP_DIR);
$loader->register();

$socket       = new Thrift\Transport\TSocket('localhost', 22222);
$transport    = new Thrift\Transport\TBufferedTransport($socket, 1024, 1024);
$socket->setSendTimeout(30000);
$socket->setRecvTimeout(30000);
$transport->open();
$protocol  = new Thrift\Protocol\TBinaryProtocol($transport);

$grader = new \Codah\Grader\GraderClient($protocol);

// try {
//     $grader->createTask('read_write');
// } catch (Throwable $e) {
//     var_dump($e);
// }

// $checker_code = file_get_contents('./solutions/5000000008/checker.cpp');

// $grader->setChecker('read_write', 0x203000, $checker_code);

try {
     $grader->removeAllTestCases('read_write');
     $grader->addTestCase('read_write', '3', '3');
     $grader->addTestCase('read_write', '1', '1');
     $grader->addTestCase('read_write', '4', '4');
     $grader->addTestCase('read_write', '5', '5');
     $grader->addTestCase('read_write', '6', '6');
     // $grader->insertTestCase('read_write', 0, '1', '1');
 } catch (Throwable $e) {
     var_dump($e);
 }



// // $code = file_get_contents('./solutions/5000000008/sol.p');

// $r = $grader->grade($code, '5000000008', 1, 1000, 16 * 1024, 3);
// var_dump($r);

$code = file_get_contents('./solutions/5000000008/zi.py');

$r = $grader->fullGrade($code, 'read_write', 0x1001000, 1000, 16 * 1024 + 64 * 1024);

$r = $grader->partialGrade($code, 'read_write', 0x1001000, 1000, 16 * 1024 + 64 * 1024, 1, 3);


var_dump($r);

// $r = $grader->getTaskState('read_write');
// var_dump($r);

// $r = $grader->getTestInput('read_write', 1);
// var_dump($r);

// $r = $grader->getTestSolution('read_write', 1);
// var_dump($r);

// $code = file_get_contents('./solutions/5000000008/sol.c');

// $r = $grader->grade($code, '5000000008', 2, 1000, 16 * 1024, 3);
// var_dump($r);
// $r = $grader->grade($code, '5000000008', 3, 1000, 16 * 1024, 3);
// var_dump($r);

// // C++ is (almost fully) compatible with C
// $r = $grader->grade($code, '5000000008', 4, 1000, 16 * 1024, 3);
// var_dump($r);
// $r = $grader->grade($code, '5000000008', 5, 1000, 16 * 1024, 3);
// var_dump($r);
// $r = $grader->grade($code, '5000000008', 6, 1000, 16 * 1024, 3);
// var_dump($r);

// for ($i = 5000000001; $i < 5000000150; ++$i) {
//     var_dump($grader->getTaskState($i));
// }
