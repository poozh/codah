#include <cstdlib>
#include <fstream>
#include <iostream>
#include <string>

#define ASSERT_CORRECT(condition)                                              \
    if (!(condition))                                                          \
    {                                                                          \
        std::cout << '0';                                                      \
        _Exit(0);                                                              \
    }                                                                          \
    static_cast<void>(0)

#define ITS_CORRECT()                                                          \
    {                                                                          \
        std::cout << '1';                                                      \
        _Exit(0);                                                              \
    }                                                                          \
    static_cast<void>(0)

int main(int argc, char* argv[])
{
    std::ifstream in_file(argv[1]);
    std::ifstream out_file(argv[2]);
    std::ifstream sol_file(argv[3]);

    while (!sol_file.eof())
    {
        std::string lhs;
        sol_file >> lhs;

        ASSERT_CORRECT(sol_file.rdstate() == 0);
        ASSERT_CORRECT(!out_file.eof());

        std::string rhs;
        out_file >> rhs;

        ASSERT_CORRECT(lhs == rhs);
    }

    ITS_CORRECT();
}
