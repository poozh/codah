#pragma once

#include "gen-cpp/geoip_locator.h"
#include "geolite2_city.hpp"
#include "types.hpp"
#include "windsor/windsor_service.hpp"
#include <algorithm>
#include <boost/lexical_cast.hpp>
#include <cstddef>
#include <fstream>
#include <json-c/json.h>
#include <map>
#include <mutex>
#include <string>
#include <utility>
#include <vector>
#include <zi/time.hpp>

namespace codah
{
namespace geoip
{

location& to_thrift_location(location& l, const geolite2_city::block* r)
{
    if (r)
    {
        l.country     = r->loc->country_iso_code;
        l.region      = r->loc->subdivision_1_name;
        l.city        = r->loc->city_name;
        l.postal_code = r->postal_code;
        l.latitude    = r->latitude;
        l.longitude   = r->longitude;
    }
    else
    {
        l.country = "na";
    }
    return l;
}

location& to_thrift_location(location& l, const location_type* r)
{
    if (r)
    {
        l.country     = r->country;
        l.region      = r->region;
        l.city        = r->city;
        l.postal_code = r->postal_code;
        l.latitude    = r->latitude;
        l.longitude   = r->longitude;
    }
    else
    {
        l.country = "na";
    }
    return l;
}

json_object* to_json_node(const location_type* r)
{
    json_object* n = json_object_new_object();

    if (r)
    {
        json_object_object_add(n, "country",
                               json_object_new_string(r->country.c_str()));
        json_object_object_add(n, "region",
                               json_object_new_string(r->region.c_str()));
        json_object_object_add(n, "city",
                               json_object_new_string(r->city.c_str()));
        json_object_object_add(n, "postal_code",
                               json_object_new_string(r->postal_code.c_str()));
        json_object_object_add(n, "latitude",
                               json_object_new_double(r->latitude));
        json_object_object_add(n, "longitude",
                               json_object_new_double(r->longitude));
    }
    else
    {
        json_object_object_add(n, "country", json_object_new_string(""));
        json_object_object_add(n, "region", json_object_new_string(""));
        json_object_object_add(n, "city", json_object_new_string(""));
        json_object_object_add(n, "postal_code", json_object_new_string(""));
        json_object_object_add(n, "latitude", json_object_new_double(0.0));
        json_object_object_add(n, "longitude", json_object_new_double(0.0));
    }

    return n;
}

json_object* to_json_node(geolite2_city::block const* r)
{
    json_object* n = json_object_new_object();

    if (r)
    {
        json_object_object_add(
            n, "country",
            json_object_new_string(r->loc->country_iso_code.c_str()));
        json_object_object_add(
            n, "region",
            json_object_new_string(r->loc->subdivision_1_name.c_str()));
        json_object_object_add(
            n, "city", json_object_new_string(r->loc->city_name.c_str()));
        json_object_object_add(n, "postal_code",
                               json_object_new_string(r->postal_code.c_str()));
        json_object_object_add(n, "latitude",
                               json_object_new_double(r->latitude));
        json_object_object_add(n, "longitude",
                               json_object_new_double(r->longitude));
    }
    else
    {
        json_object_object_add(n, "country", json_object_new_string(""));
        json_object_object_add(n, "region", json_object_new_string(""));
        json_object_object_add(n, "city", json_object_new_string(""));
        json_object_object_add(n, "postal_code", json_object_new_string(""));
        json_object_object_add(n, "latitude", json_object_new_double(0.0));
        json_object_object_add(n, "longitude", json_object_new_double(0.0));
    }

    return n;
}

class geoip_server_handler : virtual public geoip_locatorIf,
                             virtual public windsor_service
{
private:
    geolite2_city geolite_;
    locator       locator_;
    std::mutex    m_;

    zi::int64_t num_gets_;
    zi::int64_t num_multigets_;
    zi::int64_t num_ips_;
    double      total_time_;

public:
    geoip_server_handler()
        : windsor_service("GeoIp Service")
        , geolite_("./geoip/data/GeoLite2-City-CSV_20190820/"
                   "GeoLite2-City-Locations-en.csv",
                   "./geoip/data/GeoLite2-City-CSV_20190820/"
                   "GeoLite2-City-Blocks-IPv4.csv")
        , locator_("./geoip/data/GeoLiteCity_20120207/GeoLiteCity-Location.csv",
                   "./geoip/data/GeoLiteCity_20120207/GeoLiteCity-Blocks.csv")
        , m_()
        , num_gets_(0)
        , num_multigets_(0)
        , num_ips_(0)
        , total_time_(static_cast<double>(0))
    {
    }

    bool ping() { return true; }

    void get_country(std::string& _return, const std::string& ip)
    {
        zi::wall_timer t;

        // location_type const* l = locator_.get_location(ip);
        auto l = geolite_.get_location(ip);
        if (l)
        {
            _return = l->loc->country_iso_code;
        }
        else
        {
            _return = "na";
        }

        {
            std::unique_lock<std::mutex> g(m_);
            ++num_ips_;
            ++num_gets_;
            total_time_ +=
                std::max(t.elapsed<double>(), static_cast<double>(0));
        }

        std::cout << "get_country(): " << t.elapsed<double>() << "\n";
    }

    void get_countries(std::vector<std::string>&       _return,
                       const std::vector<std::string>& ips)
    {
        zi::wall_timer t;

        // std::vector<location_type const*> r;
        // locator_.get_locations(r, ips);

        std::vector<geolite2_city::block const*> r;
        geolite_.get_locations(r, ips);
        _return.resize(r.size());

        for (std::size_t i = 0; i < r.size(); ++i)
        {
            if (r[i])
            {
                _return[i] = r[i]->loc->country_iso_code;
            }
            else
            {
                _return[i] = "na";
            }
        }

        {
            std::unique_lock<std::mutex> g(m_);
            num_ips_ += ips.size();
            ++num_multigets_;
            total_time_ +=
                std::max(t.elapsed<double>(), static_cast<double>(0));
        }

        std::cout << "get_countries(" << r.size()
                  << "): " << t.elapsed<double>() << "\n";
    }

    void get_location(location& _return, const std::string& ip)
    {
        zi::wall_timer t;

        to_thrift_location(_return, geolite_.get_location(ip));

        {
            std::unique_lock<std::mutex> g(m_);
            ++num_ips_;
            ++num_gets_;
            total_time_ +=
                std::max(t.elapsed<double>(), static_cast<double>(0));
        }

        std::cout << "get_location(): " << t.elapsed<double>() << "\n";
    }

    void get_locations(std::vector<location>&          _return,
                       const std::vector<std::string>& ips)
    {
        zi::wall_timer t;

        // std::vector<location_type const*> r;
        // locator_.get_locations(r, ips);

        std::vector<geolite2_city::block const*> r;
        geolite_.get_locations(r, ips);
        _return.resize(r.size());

        for (std::size_t i = 0; i < r.size(); ++i)
        {
            to_thrift_location(_return[i], r[i]);
        }

        {
            std::unique_lock<std::mutex> g(m_);
            num_ips_ += ips.size();
            ++num_multigets_;
            total_time_ +=
                std::max(t.elapsed<double>(), static_cast<double>(0));
        }

        std::cout << "get_locations(" << r.size()
                  << "): " << t.elapsed<double>() << "\n";
    }

    void get_locations_json(std::string&                    _return,
                            const std::vector<std::string>& ips)
    {
        zi::wall_timer t;

        // std::vector<location_type const*> r;
        // locator_.get_locations(r, ips);

        std::vector<geolite2_city::block const*> r;
        geolite_.get_locations(r, ips);
        _return.resize(r.size());

        json_object* json_array = json_object_new_array();

        for (std::size_t i = 0; i < r.size(); ++i)
        {
            json_object_array_add(json_array, to_json_node(r[i]));
        }

        char const* jc = json_object_get_string(json_array);

        _return = jc;

        json_object_put(json_array);

        {
            std::unique_lock<std::mutex> g(m_);
            num_ips_ += ips.size();
            ++num_multigets_;
            total_time_ +=
                std::max(t.elapsed<double>(), static_cast<double>(0));
        }

        std::cout << "get_locations_json(" << r.size()
                  << "): " << t.elapsed<double>() << "\n";
    }

    void get_stats(std::map<std::string, std::string>& _return)
    {
        std::unique_lock<std::mutex> g(m_);
        _return["number of gets"] = boost::lexical_cast<std::string>(num_gets_);
        _return["number of multigets"] =
            boost::lexical_cast<std::string>(num_multigets_);
        _return["number of IPs"] = boost::lexical_cast<std::string>(num_ips_);
        _return["average time per IP"] =
            boost::lexical_cast<std::string>(total_time_ / num_ips_);
    }
};
}
} // namespace codah::geoip
