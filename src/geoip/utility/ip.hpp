#pragma once

#include <cstddef>
#include <sstream>
#include <string>

namespace codah
{

inline unsigned int ip_to_int(const std::string& s)
{
    unsigned int r = 0;
    unsigned int t = 0;

    for (std::size_t i = 0; i < s.size(); ++i)
    {
        if (s[i] == '.')
        {
            r <<= 8;
            r += t;
            t = 0;
        }
        else
        {
            t *= 10;
            t += static_cast<unsigned int>(s[i] - '0');
        }
    }

    return (r << 8) + t;
}

inline std::string int_to_ip(unsigned int ip)
{
    std::ostringstream oss;
    oss << (ip >> 24) << '.' << ((ip >> 16) & 0xFF) << '.' << ((ip >> 8) & 0xFF)
        << '.' << (ip & 0xFF);

    return oss.str();
}

} // namespace codah
