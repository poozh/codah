#include "geoip_server.hpp"

#include <zi/arguments.hpp>
#include <zi/system/daemon.hpp>

#include <concurrency/PosixThreadFactory.h>
#include <concurrency/ThreadManager.h>
#include <protocol/TBinaryProtocol.h>
#include <server/TThreadPoolServer.h>
#include <transport/TBufferTransports.h>
#include <transport/TServerSocket.h>

#include <boost/shared_ptr.hpp>

ZiARG_int32(port, 9090, "Server's port");
ZiARG_bool(daemonize, true, "Run as daemon");

using namespace ::apache::thrift;
using namespace ::apache::thrift::protocol;
using namespace ::apache::thrift::transport;
using namespace ::apache::thrift::server;

using apache::thrift::concurrency::ThreadManager;
using apache::thrift::concurrency::PosixThreadFactory;

using boost::shared_ptr;

int main(int argc, char** argv)
{
    zi::parse_arguments(argc, argv, true);
    if (ZiARG_daemonize)
    {
        if (!::zi::system::daemonize(true, true))
        {
            std::cerr << "Error trying to daemonize." << std::endl;
            return -1;
        }
    }

    int                                            port = ZiARG_port;
    shared_ptr<codah::geoip::geoip_server_handler> handler(
        new codah::geoip::geoip_server_handler());
    shared_ptr<TProcessor> processor(
        new codah::geoip::geoip_locatorProcessor(handler));
    shared_ptr<TServerTransport>  serverTransport(new TServerSocket(port));
    shared_ptr<TTransportFactory> transportFactory(
        new TBufferedTransportFactory());
    shared_ptr<TProtocolFactory> protocolFactory(new TBinaryProtocolFactory());

    shared_ptr<ThreadManager> threadManager(
        ThreadManager::newSimpleThreadManager(20));
    shared_ptr<PosixThreadFactory> threadFactory(new PosixThreadFactory());

    threadManager->threadFactory(threadFactory);
    threadManager->start();

    shared_ptr<TServer> server(
        new TThreadPoolServer(processor, serverTransport, transportFactory,
                              protocolFactory, threadManager));
    std::cout << "Server Ready" << std::endl;

    server->serve();
    return 0;
}
