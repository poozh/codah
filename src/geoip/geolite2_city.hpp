#pragma once

#include "utility/ip.hpp"

#include <boost/algorithm/string.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/tokenizer.hpp>

#include <fstream>
#include <iostream>
#include <map>
#include <sstream>
#include <string>
#include <utility>
#include <vector>

namespace codah
{
namespace geoip
{

inline std::pair<unsigned, unsigned> network_to_int_range(std::string net)
{

    for (auto& c : net)
    {
        if (c == '.' || c == '/')
        {
            c = ' ';
        }
    }

    std::pair<unsigned, unsigned> ret(0, 0);
    std::istringstream iss(net);

    for (int i = 0; i < 4; ++i)
    {
        unsigned x;
        iss >> x;
        ret.first = (ret.first << 8) + x;
    }

    unsigned x;
    iss >> x;

    ret.second = ret.first + (1u << (32 - x));

    return ret;
}

class geolite2_city
{
public:
    struct location
    {
        unsigned    geoname_id;
        std::string locale_code;
        std::string continent_code;
        std::string continent_name;
        std::string country_iso_code;
        std::string country_name;
        std::string subdivision_1_iso_code;
        std::string subdivision_1_name;
        std::string subdivision_2_iso_code;
        std::string subdivision_2_name;
        std::string city_name;
        std::string metro_code;
        std::string time_zone;
        bool        is_in_european_union;
    };

    struct block
    {
        std::string postal_code;
        double      latitude;
        double      longitude;
        location*   loc;
    };

private:
    std::vector<location> locations_;
    std::map<unsigned, std::pair<unsigned, block>> data_;

public:
    geolite2_city(geolite2_city const&) = delete;
    geolite2_city& operator=(geolite2_city const&) = delete;

    geolite2_city(std::string const& locations_file,
                  std::string const& blocks_file)
    {
        std::map<unsigned, unsigned> locations_map;

        {
            std::ifstream fin(locations_file.c_str());
            std::string   s;
            std::getline(fin, s);

            while (std::getline(fin, s))
            {
                using tokenizer =
                    boost::tokenizer<boost::escaped_list_separator<char>>;

                tokenizer tok{s};

                std::vector<std::string> v;

                for (const auto& t : tok)
                {
                    v.push_back(t);
                }

                if (v.size() != 14)
                {
                    std::cerr << "Wrong size :: ";
                    std::cout << s << "\n";
                }
                else
                {
                    unsigned id    = boost::lexical_cast<unsigned>(v[0]);
                    bool     in_eu = boost::lexical_cast<unsigned>(v[13]);

                    for (std::size_t i = 1; i < 14; ++i)
                    {
                        boost::algorithm::trim_if(
                            v[i], boost::algorithm::is_any_of("\""));
                    }

                    locations_map[id] =
                        static_cast<unsigned>(locations_.size());

                    locations_.push_back({id, v[1], v[2], v[3], v[4], v[5],
                                          v[6], v[7], v[8], v[9], v[10], v[11],
                                          v[12], in_eu});
                }
            }
        }

        {
            std::ifstream fin(blocks_file.c_str());
            std::string   s;
            std::getline(fin, s);

            while (std::getline(fin, s))
            {
                using tokenizer =
                    boost::tokenizer<boost::escaped_list_separator<char>>;

                tokenizer tok{s};

                std::vector<std::string> v;

                for (const auto& t : tok)
                {
                    v.push_back(t);
                }

                if (v.size() != 10)
                {
                    std::cerr << "Wrong size :: ";
                    std::cout << s << "\n";
                }
                else
                {
                    auto p = network_to_int_range(v[0]);

                    try
                    {
                        unsigned  id = boost::lexical_cast<unsigned>(v[1]);
                        double    latitude  = boost::lexical_cast<double>(v[7]);
                        double    longitude = boost::lexical_cast<double>(v[8]);
                        location* loc       = &locations_[locations_map[id]];

                        data_[p.first] = std::pair<unsigned, block>{
                            p.second, block{v[6], latitude, longitude, loc}};
                    }
                    catch (...)
                    {
                        std::cout << "CATCH: " << s << "\n";
                    }
                }
            }
        }
    }

    block const* get_location(unsigned ip) const
    {
        auto it = data_.lower_bound(ip);

        if ((it == data_.end()) || (ip >= it->second.first))
        {
            return nullptr;
        }

        return &it->second.second;
    }

    block const* get_location(std::string const& ip) const
    {
        return get_location(ip_to_int(ip));
    }

    void get_locations(std::vector<block const*>&   ret,
                       std::vector<unsigned> const& ips) const
    {
        ret.resize(ips.size());
        for (std::size_t i = 0; i < ips.size(); ++i)
        {
            ret[i] = get_location(ips[i]);
        }
    }

    void get_locations(std::vector<block const*>&      ret,
                       std::vector<std::string> const& ips) const
    {
        ret.resize(ips.size());
        for (std::size_t i = 0; i < ips.size(); ++i)
        {
            ret[i] = get_location(ips[i]);
        }
    }
};
}
}
