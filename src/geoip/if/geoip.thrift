include "windsor/if/windsor.thrift"

namespace cpp codah.geoip
namespace php Codah.Geoip

struct location
{
    1: string country,
    2: string region,
    3: string city,
    4: string postal_code,
    5: double latitude,
    6: double longitude
}

service geoip_locator extends windsor.windsor_service
{
    bool                ping(),
    location            get_location( 1: string ip ),
    list<location>      get_locations( 1: list<string> ips),
    string              get_country( 1: string ip ),
    list<string>        get_countries( 1: list<string> ips ),
    binary              get_locations_json( 1: list<string> ips ),
    map<string,string>  get_stats()
}
