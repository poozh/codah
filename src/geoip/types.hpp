#pragma once

#include <cstddef>
#include <fstream>
#include <iostream>
#include <map>
#include <string>
#include <utility>
#include <vector>

#include <boost/algorithm/string.hpp>
#include <boost/lexical_cast.hpp>

#include "utility/ip.hpp"

namespace codah
{
namespace geoip
{

typedef unsigned int ip_int;

struct location_type
{
    std::string country;
    std::string region;
    std::string city;
    std::string postal_code;
    double      latitude;
    double      longitude;
};

class locator
{
private:
    typedef std::map<ip_int, std::pair<ip_int, location_type*>> map_type;

private:
    map_type map_;

public:
    locator(const std::string& locations_file, const std::string& blocks_file)
    {
        std::map<std::size_t, location_type*> locations;
        {
            std::ifstream fin(locations_file.c_str());
            std::string   s;
            std::getline(fin, s);
            std::getline(fin, s);

            while (std::getline(fin, s))
            {
                std::vector<std::string> v;
                boost::algorithm::split(v, s, boost::algorithm::is_any_of(","));

                if (v.size() != 9)
                {
                    std::cerr << "Wrong size;\n";
                }
                else
                {
                    std::size_t id = boost::lexical_cast<std::size_t>(v[0]);

                    for (std::size_t i = 1; i < 5; ++i)
                    {
                        boost::algorithm::trim_if(
                            v[i], boost::algorithm::is_any_of("\""));
                    }

                    location_type* l = new location_type;
                    l->country       = v[1];
                    l->region        = v[2];
                    l->city          = v[3];
                    l->postal_code   = v[4];
                    l->latitude      = boost::lexical_cast<double>(v[5]);
                    l->longitude     = boost::lexical_cast<double>(v[6]);

                    locations[id] = l;

                    //                    std::cout << "Got: " << id <<
                    //                    std::endl;
                }
            }
        }

        {
            std::ifstream fin(blocks_file.c_str());
            std::string   s;
            std::getline(fin, s);
            std::getline(fin, s);

            while (std::getline(fin, s))
            {
                std::vector<std::string> v;
                boost::algorithm::split(v, s, boost::algorithm::is_any_of(","));

                if (v.size() != 3)
                {
                    std::cerr << "Wrong size;\n";
                }
                else
                {
                    for (std::size_t i = 0; i < 3; ++i)
                    {
                        boost::algorithm::trim_if(
                            v[i], boost::algorithm::is_any_of("\""));
                    }

                    ip_int      from = boost::lexical_cast<ip_int>(v[0]);
                    ip_int      to   = boost::lexical_cast<ip_int>(v[1]);
                    std::size_t loc  = boost::lexical_cast<std::size_t>(v[2]);

                    map_[to].first = from;

                    if (locations[loc] == 0)
                    {
                        std::cout << "Missing location: " << loc << std::endl;
                    }

                    map_[to].second = locations[loc];
                    // locations[loc]  = 0;
                }
            }

            std::size_t unused = 0;
            for (std::map<std::size_t, location_type*>::iterator it =
                     locations.begin();
                 it != locations.end(); ++it)
            {
                // if ( it->second != 0 )
                // {
                //     ++unused;
                //     delete it->second;
                //     it->second = 0;
                // }
            }

            if (unused > 0)
            {
                std::cout << "Earsed " << unused << " uniused locations "
                          << "out of " << locations.size() << "locations\n";
            }
        }
    }

    ~locator()
    {
        for (map_type::iterator it = map_.begin(); it != map_.end(); ++it)
        {
            delete it->second.second;
        }
    }

    location_type const* get_location(ip_int ip) const
    {
        map_type::const_iterator it = map_.lower_bound(ip);
        if (it == map_.end())
        {
            return 0;
        }
        else
        {
            if (it->second.first <= ip)
            {
                return it->second.second;
            }
            else
            {
                return 0;
            }
        }
    }

    location_type const* get_location(const std::string& ip) const
    {
        return get_location(ip_to_int(ip));
    }

    void get_locations(std::vector<location_type const*>& ret,
                       const std::vector<ip_int>&         ips) const
    {
        ret.resize(ips.size());
        for (std::size_t i = 0; i < ips.size(); ++i)
        {
            ret[i] = get_location(ips[i]);
        }
    }

    void get_locations(std::vector<location_type const*>& ret,
                       const std::vector<std::string>&    ips) const
    {
        ret.resize(ips.size());
        for (std::size_t i = 0; i < ips.size(); ++i)
        {
            ret[i] = get_location(ips[i]);
        }
    }
};

}} // namespace codah::geoip
