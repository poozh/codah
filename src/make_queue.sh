mkdir -p gen
mkdir -p bin
thrift -I ./ -r --gen cpp -o ./gen ./windsor/if/windsor.thrift
thrift -I ./ -r --gen php -o ./gen ./windsor/if/windsor.thrift
thrift -I ./ -r --gen cpp -o ./gen ./grader/if/grader.thrift
thrift -I ./ -r --gen php -o ./gen ./grader/if/grader.thrift
thrift -I ./ -r --gen cpp -o ./gen ./queue/if/queue.thrift
thrift -I ./ -r --gen php -o ./gen ./queue/if/queue.thrift
g++ -std=c++1z queue/*.cpp  gen/gen-cpp/Queue.cpp gen/gen-cpp/queue_types.cpp gen/gen-cpp/queue_constants.cpp gen/gen-cpp/Grader.cpp gen/gen-cpp/grader_types.cpp gen/gen-cpp/grader_constants.cpp gen/gen-cpp/windsor_service.cpp gen/gen-cpp/windsor_types.cpp gen/gen-cpp/windsor_constants.cpp queue/mysql/*.cpp queue/sha256/*.cpp -I./ -I./gen  -I./zi_lib -I/usr/include/thrift -lthrift -lpthread -lgflags -lglog -lmysqlcppconn -o bin/queue_server -O2 -DNDEBUG -fno-omit-frame-pointer -Wall -Wextra
