#include "submission.hpp"

namespace codah
{

constexpr int submission::IN_QUEUE;
constexpr int submission::GRADED;

} // namespace codah
