#pragma once

#include "mysql/pool.hpp"
#include "sha256/sha256.hpp"
#include <cstdint>
#include <string>

namespace codah
{

struct submission
{
    int64_t     id              = 0;
    int64_t     user_id         = 0;
    int64_t     task_id         = 0;
    int         language_id     = 0;
    int         time_limit      = 0;
    int         memory_limit    = 0;
    int64_t     venue_id        = 0;
    int64_t     submission_time = 0;
    int64_t     grading_time    = 0;
    std::string source_code_id  = "";
    std::string source_code     = "";
    int         queue_status    = 0;
    int         grade_status    = 0;
    std::string grade_details   = "";
    int         num_tested      = 0;
    int         num_solved      = 0;
    int         total_time      = 0;
    int         max_memory      = 0;
    int         solved          = 0;

    static constexpr int IN_QUEUE = 1;
    static constexpr int GRADED   = 2;

    submission() {}

    submission(int64_t _id, int64_t _task_id, int64_t _user_id,
               int32_t _language_id, int32_t _time_limit, int32_t _memory_limit,
               int32_t _num_tests, std::string const& _source,
               int32_t _timestamp)
        : id(_id)
        , user_id(_user_id)
        , task_id(_task_id)
        , language_id(_language_id)
        , time_limit(_time_limit)
        , memory_limit(_memory_limit)
        , submission_time(_timestamp)
        , source_code_id(sha256(_source))
        , source_code(_source)
        , queue_status(IN_QUEUE)
        , num_tested(_num_tests)
    {
        auto client = mysql::pool::get_client();

        client->update(
            "INSERT INTO CodahSourceCodes(id, code) VALUES (?, ?) "
            "ON DUPLICATE KEY UPDATE ref_count=ref_count+1",
            source_code_id, source_code);

        // LOG(INFO) << "Inserting code: " << r;

        client->update(
            "INSERT INTO CodahSubmissions(id, user_id, task_id, time_limit, "
            "memory_limit, language_id, submission_time, source_code_id, "
            "queue_status, num_tested) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",
            id, user_id, task_id, time_limit, memory_limit, language_id,
            submission_time, source_code_id, submission::IN_QUEUE, num_tested);

        // LOG(INFO) << "Inserting to CodahSubmissions: " << r;
    }

public:
    static std::unique_ptr<submission>
    create(int64_t id, int64_t task_id, int64_t user_id, int32_t language_id,
           int32_t time_limit, int32_t memory_limit, int32_t num_tests,
           std::string const& source, int32_t timestamp)
    {
        return std::make_unique<submission>(id, task_id, user_id, language_id,
                                            time_limit, memory_limit, num_tests,
                                            source, timestamp);
    }

    static std::unique_ptr<submission> load(int64_t id)
    {
        std::unique_ptr<submission> ret;

        auto res = mysql::pool::get_client()->query(
            "SELECT id, user_id, task_id, time_limit, "
            "memory_limit, language_id, venue_id, "
            "submission_time, grading_time, source_code_id, "
            "queue_status, grade_status, grade_details, "
            "num_tested, num_solved, total_time, max_memory, solved FROM "
            "CodahSubmissions "
            "WHERE id = ?",
            id);

        if (res->rowsCount() == 1)
        {
            res->first();
            ret                  = std::make_unique<submission>();
            ret->id              = res->getInt64(1);
            ret->user_id         = res->getInt64(2);
            ret->task_id         = res->getInt64(3);
            ret->time_limit      = res->getInt(4);
            ret->memory_limit    = res->getInt(5);
            ret->language_id     = res->getInt(6);
            ret->venue_id        = res->getInt64(7);
            ret->submission_time = res->getInt64(8);
            ret->grading_time    = res->getInt64(9);
            ret->source_code_id  = res->getString(10);
            ret->source_code     = "";
            ret->queue_status    = res->getInt(11);
            ret->grade_status    = res->getInt(12);
            ret->grade_details   = res->getInt(13);
            ret->num_tested      = res->getInt(14);
            ret->num_solved      = res->getInt(15);
            ret->total_time      = res->getInt(16);
            ret->max_memory      = res->getInt(17);
            ret->solved          = res->getInt(18);
        }

        return ret;
    }

    void update()
    {
        mysql::pool::get_client()->update(
            "UPDATE CodahSubmissions SET time_limit = ?, memory_limit = ?, "
            "grading_time = ?, queue_status = ?, grade_status "
            "= ?, grade_details = ?, num_tested = ?, num_solved = ?, "
            "total_time = ?, max_memory = ?, solved = ? WHERE id = ?",
            time_limit, memory_limit, grading_time, queue_status, grade_status,
            grade_details, num_tested, num_solved, total_time, max_memory,
            solved, id);
    }

    bool load_source()
    {
        if (source_code != "")
        {
            return true;
        }

        auto r = mysql::pool::get_client()->query(
            "SELECT code FROM CodahSourceCodes WHERE id = ?", source_code_id);

        if (r->rowsCount() > 0)
        {
            r->first();
            source_code = r->getString(1);
            return true;
        }

        return false;
    }
};

} // namespace codah
