#include "queue.hpp"
#include <glog/logging.h>
#include <thread>

namespace codah
{

constexpr int64_t queue::SENTINEL;

void queue::schedule(int level, int64_t id)
{
    std::unique_lock<std::mutex> lock(m_);
    deque_.push_back(level, id);
    if (idle_threads_ > 0)
    {
        slave_cv_.notify_one();
    }
}

queue::queue(int threads)
{
    auto cl = codah::mysql::pool::get_client();

    auto res = cl->query("SELECT MAX(id) FROM CodahSubmissions");

    res->first();

    if (res->isNull(1))
    {
        last_id_ = 7000000000ULL;
    }
    else
    {
        last_id_ = res->getInt64(1);
    }

    LOG(INFO) << "Last ID: " << last_id_;

    res = cl->query("SELECT id FROM CodahSubmissions WHERE queue_status = ?",
                    submission::IN_QUEUE);

    if (res->rowsCount())
    {
        res->first();
        for (std::size_t i = 0; i < res->rowsCount(); ++i)
        {
            LOG(INFO) << "Found queued submission " << res->getInt64(1);
            regrade_internal(res->getInt64(1), true);
            res->next();
        }
    }

    {

        std::unique_lock<std::mutex> lock(m_);

        total_threads_ = threads;
        for (int i = 0; i < threads; ++i)
        {
            std::thread t(&queue::queue_loop, this);
            t.detach();
        }

        while (num_threads_ < total_threads_)
        {
            master_cv_.wait(lock);
        }
    }
}

queue::~queue()
{
    std::unique_lock<std::mutex> lock(m_);
    for (int i = 0; i < total_threads_; ++i)
    {
        deque_.push_back(2, queue::SENTINEL);
    }

    slave_cv_.notify_all();

    while (num_threads_ > 0)
    {
        master_cv_.wait(lock);
    }
}

int64_t queue::training_submission(int64_t task_id, int64_t user_id,
                                   int32_t language_id, int32_t time_limit,
                                   int32_t memory_limit, int32_t num_tested,
                                   std::string const& source, int32_t timestamp)
{
    int priority = 2;
    if (timestamp == 0)
    {
        priority  = 0; // real time submission (temorary hack)
        timestamp = std::time(0);
    }

    int64_t submission_id = ++last_id_;

    auto entry = submissions_.get(submission_id);

    if (entry)
    {
        LOG(WARNING) << "Submission: " << submission_id << " already exists";
    }

    if ((source.size() < 65536) && (source.find('\0') == std::string::npos))
    {
        entry.assign(submission::create(submission_id, task_id, user_id,
                                        language_id, time_limit, memory_limit,
                                        num_tested, source, timestamp));
    }
    else
    {
        std::string sub_source = "";
        if (source.size() < 65536)
        {
            sub_source = source;
        }
        else
        {
            sub_source = source.substr(0, 65535);
        }

        if (sub_source.find('\0') != std::string::npos)
        {
            sub_source = "File that contains a 0 byte";
        }

        entry.assign(submission::create(submission_id, task_id, user_id,
                                        language_id, time_limit, memory_limit,
                                        num_tested, sub_source, timestamp));
    }

    schedule(priority, submission_id);

    return submission_id;
}

int64_t queue::regrade_internal(int64_t id, bool force, int32_t time_limit,
                                int32_t memory_limit, int32_t num_tested)
{
    auto entry = submissions_.get(id, true);

    if (!entry)
    {
        return 0;
    }

    bool needs_store = (entry->time_limit != time_limit) ||
                       (entry->memory_limit != memory_limit) ||
                       (entry->num_tested != num_tested) ||
                       (entry->queue_status != submission::IN_QUEUE);

    if (time_limit)
    {
        entry->time_limit = time_limit;
    }

    if (memory_limit)
    {
        entry->memory_limit = memory_limit;
    }

    if (num_tested)
    {
        entry->num_tested = num_tested;
    }

    bool already_scheduled =
        entry->queue_status == submission::IN_QUEUE && !force;

    entry->queue_status = submission::IN_QUEUE;

    if (needs_store)
    {
        entry->update();
    }

    if (already_scheduled)
    {
        return id;
    }

    if (entry->load_source())
    {
        schedule(2, id);
        return id;
    }
    else
    {
        return 0;
    }
}

int64_t queue::regrade(int64_t id, int32_t time_limit, int32_t memory_limit,
                       int32_t num_tested)
{
    return regrade_internal(id, false, time_limit, memory_limit, num_tested);
}

submissions::entry queue::get_entry(int64_t id)
{
    return submissions_.get(id, true);
}

} // namespace codah
