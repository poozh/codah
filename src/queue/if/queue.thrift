include "grader/if/grader.thrift"

namespace cpp codah.services
namespace php Codah.Queue

const i32 STATUS_NOT_FOUND  = 0
const i32 STATUS_IN_QUEUE   = 1
const i32 STATUS_GRADED     = 2

struct SubmissionDetails
{
  1: i64 id,
  2: i64 user_id,
  3: i64 task_id,
  4: i32 language_id,
  5: i32 time_limit,
  6: i32 memory_limit,
  7: i64 venue_id,
  8: i64 submission_time,
  9: i64 grading_time,
  10: binary source_code_id,
  11: i32 queue_status,
  12: i32 grade_status,
  13: binary grade_details,
  14: i32 num_tested,
  15: i32 num_solved,
  16: i32 total_time,
  17: i32 max_memory,
  18: i32 solved,
}

struct StatusReport
{
  1: bool is_found,
  2: SubmissionDetails details,
}

service Queue
{
  i64 submitTrainingTask(
    1: i64    task_id,
    2: i64    user_id,
    3: i32    language_id,
    4: i32    time_limit,
    5: i32    memory_limit,
    6: i32    num_tests,
    7: binary source,
    8: i32    timestamp,
    ),

  StatusReport getStatus(
    1: i64 id
  ),

  i64 regrade(
    1: i64  submission_id,
    2: i32  time_limit,
    3: i32  memory_limit,
    4: i32  num_tests,
  )
}