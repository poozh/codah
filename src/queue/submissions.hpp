#pragma once

#include "mysql/pool.hpp"
#include "submission.hpp"
#include <map>
#include <memory>
#include <mutex>
#include <utility>

namespace codah
{

class submissions
{
private:
    struct map_entry
    {
        std::mutex                  m;
        std::unique_ptr<submission> data = nullptr;
    };

    static const int64_t NUM_BUCKETS = 128;

public:
    // entry is like a locked pointer.  as long as an entry instance
    // exist to some map_entry (submission id) the data is locked.
    // this means that only one entry per submission id can exist.
    // entry can be created only inside the submissions class with the
    // appropriate bucket locked.

    class entry
    {
    private:
        map_entry* e_ = nullptr;

        friend class submissions;
        entry(map_entry* e)
            : e_(e)
        {
            e->m.lock();
        }

    public:
        entry() {}

        ~entry()
        {
            if (e_)
            {
                e_->m.unlock();
            }
        }

        entry(entry const&) = delete;
        entry& operator=(entry const&) = delete;

        entry(entry&& other) { e_ = std::exchange(other.e_, nullptr); };

        entry& operator=(entry&& other)
        {
            e_ = std::exchange(other.e_, nullptr);
            return *this;
        }

        void assign(std::unique_ptr<submission> s) { e_->data = std::move(s); }

        submission* get() const { return e_ ? e_->data.get() : nullptr; }

        submission* operator->() const { return e_ ? e_->data.get() : nullptr; }
        submission& operator*() const { return *e_->data.get(); }

        operator bool() const { return e_ && e_->data.get(); }
        bool operator!() const
        {
            return e_ == nullptr || e_->data.get() == nullptr;
        }
    };

private:
    std::map<int64_t, std::unique_ptr<map_entry>> maps[NUM_BUCKETS];
    std::mutex mutexes[NUM_BUCKETS];

public:
    submissions() {}

    submissions(submissions&&) = delete;
    submissions& operator=(submissions&&) = delete;
    submissions(submissions const&)       = delete;
    submissions& operator=(submissions const&) = delete;

    entry get(int64_t id, bool and_read = false)
    {
        entry      res;
        map_entry* me = nullptr;

        {

            std::unique_lock<std::mutex> lock(mutexes[id % NUM_BUCKETS]);
            auto                         r = maps[id % NUM_BUCKETS].find(id);

            if (r != maps[id % NUM_BUCKETS].end())
            {
                me = r->second.get();
            }
            else
            {
                me                         = new map_entry;
                maps[id % NUM_BUCKETS][id] = std::unique_ptr<map_entry>(me);
            }

            res = entry(me); // locks the pointer (entry)
        }

        if (and_read && (me->data == nullptr))
        {
            me->data = submission::load(id);
        }
        return res;
    }

    void clear(int64_t age = 0)
    {
        auto time_now = std::time(0);
        for (int i = 0; i < NUM_BUCKETS; ++i)
        {
            std::unique_lock<std::mutex> lock(mutexes[i]);
            std::map<int64_t, std::unique_ptr<map_entry>> new_map;
            for (auto& s : maps[i])
            {
                std::unique_lock<std::mutex> lock(s.second->m);
                if ((!s.second->data) ||
                    (s.second->data &&
                     s.second->data->queue_status == submission::GRADED &&
                     (s.second->data->grading_time < time_now - age)))
                {
                }
                else
                {
                    new_map[s.first] = std::move(s.second);
                }
            }
            new_map.swap(maps[i]);
        }
    }
};

} // namespace codah
