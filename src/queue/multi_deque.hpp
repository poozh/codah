#pragma once

#include "../utility/assert.hpp"
#include <cstddef>
#include <deque>

namespace codah
{

template <typename T, int N>
class multi_deque
{
private:
    std::deque<T> deques_[N];

public:
    std::size_t size() const
    {
        std::size_t r = 0;
        for (auto& l : deques_)
        {
            r += l.size();
        }
        return r;
    }

    T pop()
    {
        ZI_ASSERT(size() > 0);
        for (auto& l : deques_)
        {
            if (l.size())
            {
                T r = l.front();
                l.pop_front();
                return r;
            }
        }

        T r = deques_[0].front();
        deques_[0].pop_front();
        return r;
    }

    void push_back(int bin, T const& v)
    {
        ZI_ASSERT(bin < N);
        deques_[bin].push_back(v);
    }

}; // class multi_deque

} // namespace codah
