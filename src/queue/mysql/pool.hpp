#pragma once

#include "client.hpp"
#include <condition_variable>
#include <glog/logging.h>
#include <iostream>
#include <memory>
#include <mutex>

namespace codah
{
namespace mysql
{

class pool
{
private:
    static constexpr int NUM_CLIENTS = 128;

    class pool_impl
    {
    private:
        client*                 clients_[NUM_CLIENTS];
        std::mutex              mutex_;
        std::condition_variable cv_;
        int                     available_;
        int                     waiters_;

    public:
        pool_impl()
        {
            waiters_   = 0;
            available_ = NUM_CLIENTS;
            for (int i = 0; i < NUM_CLIENTS; ++i)
            {
                clients_[i] = new client;
            }
        }

        ~pool_impl()
        {
            for (int i = 0; i < NUM_CLIENTS; ++i)
            {
                delete clients_[i];
            }
        }

        pool_impl(pool_impl&&) = delete;
        pool_impl& operator=(pool_impl&&) = delete;
        pool_impl(pool_impl const&)       = delete;
        pool_impl& operator=(pool_impl const&) = delete;

        client* get_client()
        {
            std::unique_lock<std::mutex> lock(mutex_);

            while (available_ == 0)
            {
                ++waiters_;
                cv_.wait(lock);
                --waiters_;
            }

            return clients_[--available_];
        }

        void return_client(client* c)
        {
            std::unique_lock<std::mutex> lock(mutex_);

            clients_[available_++] = c;
            if (waiters_)
            {
                cv_.notify_one();
            }
        }
    };

private:
    static pool_impl static_instance_;

    static void return_client(client* c) { static_instance_.return_client(c); }

private:
    using client_ptr = std::unique_ptr<client, decltype(&pool::return_client)>;

    pool() {}

public:
    static client_ptr get_client()
    {
        client* c = static_instance_.get_client();
        return client_ptr(c, &pool::return_client);
    }

}; // class pool

} // namespace mysql
} // namespace codah
