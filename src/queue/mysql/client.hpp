#pragma once

#include <cppconn/driver.h>
#include <cppconn/exception.h>
#include <cppconn/prepared_statement.h>
#include <cppconn/resultset.h>
#include <cppconn/statement.h>
#include <gflags/gflags.h>
#include <glog/logging.h>
#include <memory>
#include <mutex>
#include <unistd.h>

DECLARE_string(database_name);

namespace codah
{
namespace mysql
{

class client
{
private:
    std::unique_ptr<sql::Connection>        connection_;
    std::unique_ptr<sql::PreparedStatement> statement_;

    static std::mutex static_mutex_;

private:
    template <typename... Args>
    void create_statement(std::string const& sql_string, Args const&... args)
    {
        statement_ = std::unique_ptr<sql::PreparedStatement>(
            connection_->prepareStatement(sql_string.c_str()));
        build_statement_helper(1, args...);
    }

    void build_statement_helper(unsigned int) {}

    template <typename T, typename... Ts>
    void build_statement_helper(unsigned int n, T const& v, Ts const&... vs)
    {
        bind_single(n, v);
        build_statement_helper(n + 1, vs...);
    }

    template <typename T>
    std::enable_if_t<std::is_floating_point<T>::value && (sizeof(T) == 4)>
    bind_single(unsigned int n, T v)
    {
        statement_->setDouble(n, v);
    }

    template <typename T>
    std::enable_if_t<std::is_signed<T>::value && (sizeof(T) == 4)>
    bind_single(unsigned int n, T v)
    {
        statement_->setInt(n, v);
    }

    template <typename T>
    std::enable_if_t<std::is_signed<T>::value && (sizeof(T) == 8)>
    bind_single(unsigned int n, T v)
    {
        statement_->setInt64(n, v);
    }

    template <typename T>
    std::enable_if_t<std::is_unsigned<T>::value && (sizeof(T) <= 4)>
    bind_single(unsigned int n, T v)
    {
        statement_->setUInt(n, v);
    }

    template <typename T>
    std::enable_if_t<std::is_unsigned<T>::value && (sizeof(T) == 8)>
    bind_single(unsigned int n, T v)
    {
        statement_->setUInt64(n, v);
    }

    void bind_single(unsigned int n, std::string const& v)
    {
        statement_->setString(n, v);
    }

    void connect()
    {
        if (!connection_)
        {
            sql::Connection* con = nullptr;
            {
                std::unique_lock<std::mutex> l(static_mutex_);
                con = get_driver_instance()->connect("tcp://127.0.0.1:3306",
                                                     "root", "perica");
            }
            con->setSchema(FLAGS_database_name.c_str());
            connection_ = std::unique_ptr<sql::Connection>(con);
        }
        else if (!connection_->isValid())
        {
            LOG(WARNING) << "Connection not valid, reconnecting";
            connection_->reconnect();
            connection_->setSchema(FLAGS_database_name.c_str());
        }
    }

public:
    template <typename... Args>
    std::unique_ptr<sql::ResultSet> query(std::string const& sql_string,
                                          Args const&... args)
    {
        for (;;)
        {
            try
            {
                connect();
                create_statement(sql_string, args...);
                sql::ResultSet* res = statement_->executeQuery();
                return std::unique_ptr<sql::ResultSet>(res);
            }
            catch (sql::SQLException& e)
            {
                LOG(WARNING) << "SQLException in " << __FILE__ << "("
                             << __FUNCTION__ << ") on line " << __LINE__
                             << ", # ERR: " << e.what()
                             << " (MySQL error code: " << e.getErrorCode()
                             << ", SQLState: " << e.getSQLState() << " )";
                sleep(2);
            }
        }
    }

    template <typename... Args>
    int update(std::string const& sql_string, Args const&... args)
    {
        for (;;)
        {
            try
            {
                connect();
                create_statement(sql_string, args...);
                return statement_->executeUpdate();
            }
            catch (sql::SQLException& e)
            {
                LOG(WARNING) << "SQLException in " << __FILE__ << "("
                             << __FUNCTION__ << ") on line " << __LINE__
                             << ", # ERR: " << e.what()
                             << " (MySQL error code: " << e.getErrorCode()
                             << ", SQLState: " << e.getSQLState() << " )";
                sleep(2);
            }
        }
    }

}; // class mysql

} // namespace mysql
} // namespace codah
