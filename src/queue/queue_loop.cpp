#include "../gen/gen-cpp/Grader.h"
#include "../grader/grader/grader.hpp"
#include "queue.hpp"

#include <thrift/protocol/TBinaryProtocol.h>
#include <thrift/transport/TBufferTransports.h>
#include <thrift/transport/TSocket.h>

namespace codah
{

void queue::queue_loop()
{

    using apache::thrift::TException;
    using apache::thrift::transport::TSocket;
    using apache::thrift::transport::TBufferedTransport;
    using apache::thrift::transport::TTransport;
    using apache::thrift::protocol::TBinaryProtocol;
    using apache::thrift::protocol::TProtocol;

    auto socket_ = boost::shared_ptr<TSocket>(new TSocket("localhost", 9876));
    auto transport_ =
        boost::shared_ptr<TTransport>(new TBufferedTransport(socket_));
    auto protocol_ =
        boost::shared_ptr<TProtocol>(new TBinaryProtocol(transport_));

    services::GraderClient client(protocol_);

    {
        std::unique_lock<std::mutex> lock(m_);
        ++num_threads_;
        if (num_threads_ == total_threads_)
        {
            master_cv_.notify_one();
        }
    }

    for (;;)
    {
        int64_t submission_id;

        {
            std::unique_lock<std::mutex> lock(m_);
            while (deque_.size() == 0)
            {
                ++idle_threads_;
                slave_cv_.wait(lock);
                --idle_threads_;
            }
            submission_id = deque_.pop();
        }

        if (submission_id == -1)
        {
            --num_threads_;
            if (num_threads_ == 0)
            {
                master_cv_.notify_one();
            }
            return;
        }

        auto entry = submissions_.get(submission_id);

        LOG(INFO) << "GRADING ID: " << submission_id;

        while (1)
        {
            try
            {
                LOG(INFO) << "1 GRADING ID: " << submission_id;
                if (!transport_->isOpen())
                {
                    transport_->open();
                }

                LOG(INFO) << "2 GRADING ID: " << submission_id;

                services::GradeReport report;
                client.grade(report, entry->source_code,
                             std::to_string(entry->task_id), entry->language_id,
                             entry->time_limit, entry->memory_limit,
                             entry->num_tested);

                LOG(INFO) << "3 GRADING ID: " << submission_id;

                entry->grading_time = std::time(0);
                entry->grade_status = report.status;
                entry->queue_status = submission::GRADED;
                entry->solved       = 0;

                if (report.status == 0)
                {
                    entry->num_solved = 0;
                    entry->total_time = 0;
                    entry->max_memory = 0;

                    std::ostringstream oss;
                    oss << '[';

                    for (int i = 0; i < entry->num_tested; ++i)
                    {
                        entry->num_solved +=
                            report.test_cases[i].solved == 0 ? 1 : 0;
                        entry->total_time += report.test_cases[i].time;
                        entry->max_memory = std::max(
                            entry->max_memory, report.test_cases[i].memory);
                        oss << (i == 0 ? "[" : ",[")
                            << report.test_cases[i].status << ','
                            << report.test_cases[i].time << ','
                            << report.test_cases[i].memory << ','
                            << report.test_cases[i].solved << ']';
                    }
                    oss << ']';
                    entry->grade_details = oss.str();
                    entry->solved =
                        (entry->num_solved == entry->num_tested) ? 1 : 0;
                }
                else
                {
                    entry->grade_details = report.status_message;
                }
                LOG(INFO) << "4 GRADING ID: " << submission_id;

                entry->update();
                LOG(INFO) << "GRADING ID: " << submission_id << " STATUS: "
                          << entry->grade_status;
                break;
            }
            catch (TException& tx)
            {
                LOG(ERROR) << "ERROR: " << tx.what();
                sleep(2);
            }
        }
    }
}

} // namespace codah
