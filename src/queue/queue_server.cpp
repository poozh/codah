#include "gen/gen-cpp/Queue.h"
#include "gen/gen-cpp/queue_constants.h"
#include "queue.hpp"

#include <boost/shared_ptr.hpp>
#include <gflags/gflags.h>
#include <glog/logging.h>
#include <iostream>
#include <thrift/concurrency/PosixThreadFactory.h>
#include <thrift/concurrency/ThreadManager.h>
#include <thrift/protocol/TBinaryProtocol.h>
#include <thrift/server/TThreadedServer.h>
#include <thrift/transport/TBufferTransports.h>
#include <thrift/transport/TServerSocket.h>
#include <zi/system/daemon.hpp>

DEFINE_string(database_name, "zdb",
              "Name of the database having the CodahSubmission table");
DEFINE_int32(port, 9877, "Server's port");
DEFINE_int32(threads, 2, "Queue threads");
DEFINE_bool(daemonize, false, "Run as daemon");
DEFINE_string(grader_host, "localhost", "Host where Grader server is running");
DEFINE_int32(grader_port, 9876, "Server's port");

using namespace ::apache::thrift;
using namespace ::apache::thrift::protocol;
using namespace ::apache::thrift::transport;
using namespace ::apache::thrift::server;

using apache::thrift::concurrency::ThreadManager;
using apache::thrift::concurrency::PosixThreadFactory;

using boost::shared_ptr;

using namespace ::codah::services;

class QueueHandler : virtual public QueueIf
{
private:
    codah::queue queue_;

public:
    QueueHandler()
        : queue_(FLAGS_threads)
    {
        // Your initialization goes here
    }

    int64_t
    submitTrainingTask(const int64_t task_id, const int64_t user_id,
                       const int32_t language_id, const int32_t time_limit,
                       const int32_t memory_limit, const int32_t num_tests,
                       const std::string& source, const int32_t timestamp)
    {

        return queue_.training_submission(task_id, user_id, language_id,
                                          time_limit, memory_limit, num_tests,
                                          source, timestamp);
    }

    int64_t regrade(const int64_t id, const int32_t time_limit,
                    const int32_t memory_limit, const int32_t num_tests)
    {
        return queue_.regrade(id, time_limit, memory_limit, num_tests);
    }

    void getStatus(StatusReport& _return, const int64_t id)
    {
        auto e = queue_.get_entry(id);
        if (e)
        {
            _return.is_found                = true;
            _return.details.id              = id;
            _return.details.user_id         = e->user_id;
            _return.details.task_id         = e->task_id;
            _return.details.language_id     = e->language_id;
            _return.details.time_limit      = e->time_limit;
            _return.details.memory_limit    = e->memory_limit;
            _return.details.venue_id        = e->venue_id;
            _return.details.submission_time = e->submission_time;
            _return.details.grading_time    = e->grading_time;
            _return.details.source_code_id  = e->source_code_id;
            _return.details.queue_status    = e->queue_status;
            _return.details.grade_status    = e->grade_status;
            _return.details.grade_details   = e->grade_details;
            _return.details.num_tested      = e->num_tested;
            _return.details.num_solved      = e->num_solved;
            _return.details.total_time      = e->total_time;
            _return.details.max_memory      = e->max_memory;
            _return.details.solved          = e->solved;
        }
        else
        {
            _return.is_found = false;
        }
    }
};

int main(int argc, char** argv)
{
    google::ParseCommandLineFlags(&argc, &argv, true);
    google::InitGoogleLogging(argv[0]);

    if (FLAGS_daemonize)
    {
        if (!::zi::system::daemonize(true, true))
        {
            std::cerr << "Error trying to daemonize." << std::endl;
            return -1;
        }
    }

    int                           port = FLAGS_port;
    shared_ptr<QueueHandler>      handler(new QueueHandler());
    shared_ptr<TProcessor>        processor(new QueueProcessor(handler));
    shared_ptr<TServerTransport>  serverTransport(new TServerSocket(port));
    shared_ptr<TTransportFactory> transportFactory(
        new TBufferedTransportFactory());
    shared_ptr<TProtocolFactory> protocolFactory(new TBinaryProtocolFactory());

    shared_ptr<TServer> server(new TThreadedServer(
        processor, serverTransport, transportFactory, protocolFactory));

    LOG(INFO) << "Server Ready";

    server->serve();
    return 0;
}
