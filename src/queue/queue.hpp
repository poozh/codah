#pragma once

#include "multi_deque.hpp"
#include "submission.hpp"
#include "submissions.hpp"

#include <atomic>
#include <condition_variable>
#include <cstdint>
#include <mutex>
#include <string>

namespace codah
{

class queue
{
private:
    static constexpr int64_t SENTINEL = -1;

    std::atomic<int64_t> last_id_;

    multi_deque<int64_t, 3> deque_;
    std::condition_variable master_cv_{}, slave_cv_{};
    std::mutex              m_{};

    int total_threads_ = 0;
    int num_threads_   = 0;
    int idle_threads_  = 0;

    submissions submissions_;

private:
    void queue_loop();
    void schedule(int level, int64_t id);

    int64_t regrade_internal(int64_t id, bool force = false,
                             int32_t time_limit = 0, int32_t memory_limit = 0,
                             int32_t num_tests = 0);

public:
    queue(int threads);
    ~queue();

    int64_t training_submission(int64_t task_id, int64_t user_id,
                                int32_t language_id, int32_t time_limit,
                                int32_t memory_limit, int32_t num_tests,
                                std::string const& source,
                                int32_t            timestamp = 0);

    int64_t regrade(int64_t id, int32_t time_limit = 0,
                    int32_t memory_limit = 0, int32_t num_tests = 0);

    submissions::entry get_entry(int64_t id);
}; // class queue

} // namespace codah
