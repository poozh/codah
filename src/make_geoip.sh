mkdir -p gen
mkdir -p bin
thrift -I ./ -r --gen cpp -o ./gen ./windsor/if/windsor.thrift
thrift -I ./ -r --gen php -o ./gen ./windsor/if/windsor.thrift
thrift -I ./ -r --gen cpp -o ./gen ./geoip/if/geoip.thrift
thrift -I ./ -r --gen php -o ./gen ./geoip/if/geoip.thrift
g++ -std=c++14 geoip/server.cpp gen/gen-cpp/geoip_locator.cpp gen/gen-cpp/geoip_types.cpp gen/gen-cpp/geoip_constants.cpp gen/gen-cpp/windsor_service.cpp gen/gen-cpp/windsor_types.cpp gen/gen-cpp/windsor_constants.cpp -I./ -I./gen  -I./zi_lib -I/usr/include/thrift -lthrift -lpthread -o bin/geoip_server -O2 -DNDEBUG -fno-omit-frame-pointer -ljson-c
