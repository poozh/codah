#pragma once

#include "sandbox.hpp"
#include "task.hpp"

#include <condition_variable>
#include <functional>
#include <list>
#include <mutex>

namespace codah
{

class sandbox_group
{
private:
    using task_type = std::function<void(sandbox const*)>;

    task_type sentinel_{};

    std::list<task_type*> tasks_{};

    std::condition_variable master_cv_{}, slave_cv_{};
    std::mutex              m_{};

    int total_boxes_ = 0;
    int num_boxes_   = 0;
    int idle_boxes_  = 0;

public:
    sandbox_group(sandbox_group const&) = delete;
    sandbox_group& operator=(sandbox_group const&) = delete;

    sandbox_group(int total_boxes, std::string const& isolate);
    ~sandbox_group();

    void box_loop(int id, std::string const& isolate);

    template <class T>
    sandbox_group& operator<<(T&& fn)
    {
        std::unique_lock<std::mutex> lock(m_);
        tasks_.push_back(new task_type(std::forward<T>(fn)));
        if (idle_boxes_ > 0)
        {
            slave_cv_.notify_one();
        }
        return *this;
    }
};

} // namespace codah
