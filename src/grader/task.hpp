#pragma once

#include "grader/grader.hpp"

#include "file_content.hpp"
#include <mutex>
#include <shared_mutex>
#include <string>
#include <utility>

namespace codah
{

class task : public std::shared_mutex
{
public:
    using shared_lock = std::shared_lock<std::shared_mutex>;
    using unique_lock = std::unique_lock<std::shared_mutex>;

private:
    std::string name_;
    std::string path_;

    compiler_type checker_lang_         = compiler_type::NONE;
    bool          has_compiled_checker_ = false;

    std::vector<test_case_state> test_cases_;

    bool is_locked_ = false;

private:
    std::string lock_file() const { return path_ + "/.lock"; }

    std::string test_file(std::size_t const no) const
    {
        return path_ + "/" + name_ + (no < 10 ? ".0" : ".") +
               std::to_string(no);
    }

    std::string input_file(std::size_t const no) const
    {
        return test_file(no) + ".in";
    }

    std::string solution_file(std::size_t const no) const
    {
        return test_file(no) + ".sol";
    }

public:
    task(task const&) = delete;
    task& operator=(task const&) = delete;

    task(std::string const& path, std::string const& name);

    std::string const& name() const { return name_; }
    std::string const& path() const { return path_; }

    bool has_checker() const { return checker_lang_ != compiler_type::NONE; }
    bool has_compiled_checker() const { return has_compiled_checker_; }
    std::size_t num_tests() const { return test_cases_.size(); }

    compiler_type get_checker_lang() const { return checker_lang_; }

    bool is_locked() const { return is_locked_; }

    file_content get_test_input(std::size_t const) const;
    file_content get_test_solution(std::size_t const) const;

    file_content get_checker() const;
    file_content get_checker_source() const;
    std::string  get_checker_source_path(compiler_type kind) const;

    bool add_test(std::string const& in, std::string const& sol);
    bool insert_test(std::size_t const no, std::string const& in,
                     std::string const& sol);
    bool remove_test(std::size_t const);
    bool remove_all_tests();

    bool set_checker_source(std::string const& code, compiler_type lang,
                            file_content const& bin);

    std::pair<bool, std::string>
    get_test_input_as_string(std::size_t const) const;
    std::pair<bool, std::string>
    get_test_solution_as_string(std::size_t const) const;

    bool lock();
    bool unlock();

    std::vector<test_case_state> const& test_cases() const
    {
        return test_cases_;
    }

    bool has_test(std::size_t no) const { return (no < test_cases_.size()); }

private:
    file_content get_test_in_or_sol(std::size_t const,
                                    std::string const&) const;
    std::pair<bool, std::string>
    get_test_in_or_sol_as_string(std::size_t const, std::string const&) const;

}; // class task

} // namespace codah
