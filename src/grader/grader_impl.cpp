#include "grader_impl.hpp"

#include "baton.hpp"
#include "fs_utils.hpp"

#include <glog/logging.h>

#include <atomic>
#include <iostream>
#include <optional>
#include <string>
#include <thread>

namespace codah
{

using namespace std::string_literals;

grader_impl::grader_impl(std::string const& path, std::size_t const num_boxes,
                         std::string const& isolate_path,
                         bool               recompile_checkers)
    : boxes_(num_boxes, isolate_path)
    , path_(path)
{
    auto l = fs_utils::ls_dirs(path);

    for (auto const& t : l)
    {
        tasks_[t] = std::make_unique<task>(path + "/" + t, t);
    }

    if (recompile_checkers)
    {
        shared_lock        rlock(map_mutex_);
        std::vector<task*> tasks_to_update;
        tasks_to_update.reserve(tasks_.size());

        for (auto const& t : tasks_)
        {
            tasks_to_update.push_back(t.second.get());
        }

        std::cout << "Recompiling Checkers!" << std::endl;

        auto const& recompile_checker = [&](std::size_t i) {
            std::cout << "For task " << i << " ";

            auto              t = tasks_to_update[i];
            task::unique_lock task_lock(*t);

            if (t->has_checker())
            {
                std::cout << "HAS GRADER: "
                          << compiler_traits::checker_extension(
                                 (t->get_checker_lang()))
                          << ' '; // << t->get_checker_source().as_string()
                //<< "\n\n";

                set_checker_no_lock(t, t->get_checker_source().as_string(),
                                    t->get_checker_lang());
                std::cout << std::endl;
            }
            else
            {
                std::cout << "NO GRADER" << std::endl;
            }
        };

        alignas(128) std::atomic<std::size_t> current{0};
        alignas(128) std::vector<std::thread> threads;

        for (unsigned int i = 0; i < std::thread::hardware_concurrency(); ++i)
        {
            threads.push_back(std::thread([&]() {
                while (true)
                {
                    auto i = current++;
                    if (i < tasks_to_update.size())
                    {
                        recompile_checker(i);
                    }
                    else
                    {
                        return;
                    }
                }
            }));
        }

        for (auto& t : threads)
        {
            t.join();
        }
    }
}

task* grader_impl::find_task(std::string const& task_name)
{
    shared_lock rlock(map_mutex_);
    auto const  cti = tasks_.find(task_name);

    if (cti == tasks_.end())
    {
        LOG(WARNING) << "Trying to find a non-existing task: " << task_name;
        return nullptr;
    }

    return cti->second.get();
}

grade_report grader_impl::partial_grade_no_lock(
    task* t, std::string const& code, compiler_type lang,
    std::size_t const time_limit, std::size_t const memory_limit,
    std::size_t const first_test, std::size_t const last_test)
{
    submission s(boxes_, *t, code, lang, time_limit, memory_limit, 64 * 1024,
                 first_test, last_test);

    return s.get_report();
}

std::pair<std::optional<std::string>, grade_report> grader_impl::partial_grade(
    std::string const& task_name, std::string const& code, compiler_type lang,
    std::size_t const time_limit, std::size_t const memory_limit,
    std::size_t const first_test, std::size_t const last_test)
{
    auto t = find_task(task_name);
    if (t == nullptr)
    {
        return {"Task not found"s, {}};
    }

    task::shared_lock task_lock(*t);

    auto n_tests = t->num_tests();

    if (first_test >= n_tests || last_test >= n_tests || first_test > last_test)
    {
        return {"Some tests missing", {}};
    }

    return {std::nullopt,
            partial_grade_no_lock(t, code, lang, time_limit, memory_limit,
                                  first_test, last_test)};
}

std::pair<std::optional<std::string>, grade_report>
grader_impl::full_grade(std::string const& task_name, std::string const& code,
                        compiler_type lang, std::size_t const time_limit,
                        std::size_t const memory_limit)
{
    auto t = find_task(task_name);
    if (t == nullptr)
    {
        return {"Task not found"s, {}};
    }

    task::shared_lock task_lock(*t);

    auto n_tests = t->num_tests();

    if (n_tests == 0)
    {
        return {"No tests found", {}};
    }

    return {std::nullopt, partial_grade_no_lock(t, code, lang, time_limit,
                                                memory_limit, 0, n_tests - 1)};
}

std::pair<bool, std::string>
grader_impl::get_test_input(std::string const& task_name,
                            std::size_t const  no) const
{
    task* t = nullptr;

    {
        shared_lock rlock(map_mutex_);
        auto const  cti = tasks_.find(task_name);

        if (cti == tasks_.end())
        {
            LOG(WARNING)
                << "Trying to acces an input file of a non-existing task: "
                << task_name << ' ' << no;
            return {false, "Task doesn't exist"};
        }
        else
        {
            t = cti->second.get();
        }
    }

    task::shared_lock lock(*t);
    return t->get_test_input_as_string(no);
}

std::pair<bool, std::string>
grader_impl::get_test_solution(std::string const& task_name,
                               std::size_t const  no) const
{
    task* t = nullptr;

    {
        shared_lock rlock(map_mutex_);
        auto const  cti = tasks_.find(task_name);

        if (cti == tasks_.end())
        {
            LOG(WARNING)
                << "Trying to acces a solution file of a non-existing task: "
                << task_name << ' ' << no;
            return {false, "Task doesn't exist"};
        }
        else
        {
            t = cti->second.get();
        }
    }

    task::shared_lock lock(*t);
    return t->get_test_solution_as_string(no);
}

std::optional<std::string>
grader_impl::set_checker(std::string const& task_name, std::string const& code,
                         compiler_type lang)
{

    if (lang == compiler_type::NONE)
    {
        return "Language not supported"s;
    }

    auto t = find_task(task_name);
    if (t == nullptr)
    {
        return "Task not found";
    }

    task::unique_lock lock(*t);
    return set_checker_no_lock(t, code, lang);
}

std::optional<std::string>
grader_impl::set_checker_no_lock(task* t, std::string const& code,
                                 compiler_type lang)
{
    baton btn;

    std::optional<std::string> ret = std::nullopt;

    boxes_ << [&](sandbox const* b) {
        auto r = b->compile(code, lang);

        if (r.first)
        {
            std::cout << "COMPILED OK ";

            if (!t->set_checker_source(code, lang, r.second))
            {
                ret = "Internal grader error (after succesful compilation"s;
            }
        }
        else
        {
            std::cout << "COMPILE ERROR ";
            ret = "Compile error:\n" + r.second.as_string(1024);
            std::cout << *ret << " :: ";
        }

        btn.notify();
    };

    btn.wait();

    return ret;
}

std::pair<std::optional<std::string>, task_state>
grader_impl::get_task_state(std::string const& task_name)
{
    auto t = find_task(task_name);
    if (t == nullptr)
    {
        return {"Task not found"s, {}};
    }

    task::shared_lock task_lock(*t);

    return {std::nullopt,
            task_state{t->name(), t->is_locked(),
                       static_cast<std::int32_t>(t->get_checker_lang()),
                       t->has_compiled_checker(), t->test_cases()}};
}

std::tuple<std::optional<std::string>, compiler_type, std::string>
grader_impl::get_checker(std::string const& task_name)
{
    auto t = find_task(task_name);
    if (t == nullptr)
    {
        return {"Task not found"s, compiler_type::NONE, ""s};
    }

    task::shared_lock task_lock(*t);

    if (!t->has_checker())
    {
        return {std::nullopt, compiler_type::NONE, ""s};
    }

    return {std::nullopt, t->get_checker_lang(),
            t->get_checker_source().as_string()};
}

std::optional<std::string> grader_impl::lock_task(std::string const& task_name)
{
    auto t = find_task(task_name);
    if (t == nullptr)
    {
        return "Task not found";
    }

    task::unique_lock task_lock(*t);

    if (t->num_tests() == 0)
    {
        return "Can't lock task with 0 test cases";
    }

    if (t->get_checker_lang() == compiler_type::NONE)
    {
        return "Can't lock task with no checker";
    }

    if (!t->has_compiled_checker())
    {
        return "Can't lock task with no compiled checker";
    }

    if (t->lock())
    {
        return std::nullopt;
    }

    return "Can't lock the task";
}

std::optional<std::string>
grader_impl::unlock_task(std::string const& task_name)
{
    auto t = find_task(task_name);
    if (t == nullptr)
    {
        return "Task not found";
    }

    task::unique_lock task_lock(*t);

    if (t->unlock())
    {
        return std::nullopt;
    }

    return "Can't unlock the task";
}

std::optional<std::string>
grader_impl::create_task(std::string const& task_name)
{
    unique_lock wlock(map_mutex_);

    if (tasks_.count(task_name))
    {
        return "Task already exists"s;
    }
    else
    {
        int status = ::mkdir(task_path(task_name).c_str(), 0744);
        if (status == 0)
        {
            tasks_[task_name] =
                std::make_unique<task>(task_path(task_name), task_name);
            return std::nullopt;
        }
        else
        {
            return "mkdir failed";
        }
    }
}

std::optional<std::string> grader_impl::add_test(std::string const& task_name,
                                                 std::string const& in_data,
                                                 std::string const& sol_data)
{
    auto t = find_task(task_name);
    if (t == nullptr)
    {
        return "Task not found";
    }

    task::unique_lock task_lock(*t);

    if (t->is_locked())
    {
        return "Task is locked";
    }

    std::cout << "ADDING IN _ : " << in_data << "\n";
    std::cout << "ADDING SOL _ : " << sol_data << "\n";

    if (t->add_test(in_data, sol_data))
    {
        return std::nullopt;
    }

    return "Can't create test files"s;
}

std::optional<std::string>
grader_impl::remove_test(std::string const& task_name, std::size_t no)
{
    auto t = find_task(task_name);
    if (t == nullptr)
    {
        return "Task not found";
    }

    task::unique_lock task_lock(*t);

    if (t->is_locked())
    {
        return "Task is locked";
    }

    if (t->remove_test(no))
    {
        return std::nullopt;
    }

    return "Can't remove the test"s;
}

std::optional<std::string>
grader_impl::remove_all_tests(std::string const& task_name)
{
    auto t = find_task(task_name);
    if (t == nullptr)
    {
        return "Task not found";
    }

    task::unique_lock task_lock(*t);

    if (t->is_locked())
    {
        return "Task is locked";
    }

    if (t->remove_all_tests())
    {
        return std::nullopt;
    }

    return "Can't remove all tests - Need to check tasks's consistency"s;
}

std::optional<std::string>
grader_impl::insert_test(std::string const& task_name, std::size_t no,
                         std::string const& in_data,
                         std::string const& sol_data)
{
    auto t = find_task(task_name);
    if (t == nullptr)
    {
        return "Task not found";
    }

    task::unique_lock task_lock(*t);

    if (t->is_locked())
    {
        return "Task is locked";
    }

    if (no > t->num_tests())
    {
        return "Bad test number - can't insert at that position";
    }

    if (t->insert_test(no, in_data, sol_data))
    {
        return std::nullopt;
    }

    return "Can't insert the test - Need to check tasks's consistency"s;
}

} // namespace codah
