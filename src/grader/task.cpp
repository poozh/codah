#include "task.hpp"
#include "fs_utils.hpp"
#include "grader/grader.hpp"

#include <glog/logging.h>
#include <iostream>

namespace codah
{

task::task(std::string const& path, std::string const& name)
    : name_(name)
    , path_(path)
{
    is_locked_ = fs_utils::file_exist(lock_file());

    has_compiled_checker_ =
        fs_utils::file_exist(path + "/" + name + ".checker");

    for (auto const& cp : compiler_traits::get_checker_langs())
    {
        if (fs_utils::file_exist(path + "/" + name + ".checker" + cp.second))
        {
            checker_lang_ = cp.first;
            break;
        }
    }

    for (std::size_t i = 0; i < 100; ++i)
    {
        auto input_path    = input_file(i);
        auto solution_path = solution_file(i);

        auto input_size = fs_utils::get_file_size(input_path);

        if (input_size)
        {
            test_case_state test_state{0, 0};

            test_state.input_size = static_cast<int>(*input_size);

            auto solution_size = fs_utils::get_file_size(solution_path);
            if (solution_size)
            {
                test_state.solution_size = *solution_size;
            }
            else
            {
                fs_utils::touch_file(solution_path);
            }

            test_cases_.push_back(test_state);
        }
    }

    LOG(INFO) << "Task: " << name << " loaded: "
              << "\t graders:" << (has_compiled_checker_ ? " [compiled]" : "")
              << "\t source: "
              << (checker_lang_ != compiler_type::NONE
                      ? compiler_traits::checker_extension(checker_lang_)
                      : "")
              << "\t tests: " << test_cases_.size() << " tests ";
}

file_content task::get_test_in_or_sol(std::size_t const  no,
                                      std::string const& ext) const
{
    if (no < num_tests())
    {
        return file_content(test_file(no) + ext);
    }

    return file_content();
}

file_content task::get_test_input(std::size_t const no) const
{
    return get_test_in_or_sol(no, ".in");
}

file_content task::get_test_solution(std::size_t const no) const
{
    return get_test_in_or_sol(no, ".sol");
}

file_content task::get_checker() const
{
    return file_content(path_ + "/" + name_ + ".checker");
}

file_content task::get_checker_source() const
{
    if (has_checker())
    {
        auto file = compiler_traits::checker_extension(
            path_ + "/" + name_ + ".checker", checker_lang_);
        return file_content(file);
    }

    return {};
}

std::string task::get_checker_source_path(compiler_type kind) const
{
    return compiler_traits::extension(path_ + "/" + name_ + ".checker", kind);
}

bool task::add_test(std::string const& in, std::string const& sol)
{
    std::string in_name  = input_file(num_tests());
    std::string sol_name = solution_file(num_tests());

    bool ok = file_content::save_string(in, in_name, 0660) &&
              file_content::save_string(sol, sol_name, 0660);

    if (ok)
    {
        test_cases_.push_back(
            {static_cast<int>(in.size()), static_cast<int>(sol.size())});
    }

    return ok;
}

bool task::insert_test(std::size_t const no, std::string const& in,
                       std::string const& sol)
{
    if (no == num_tests())
    {
        return add_test(in, sol);
    }

    if (no > num_tests())
    {
        return false;
    }

    test_cases_.resize(test_cases_.size() + 1);

    for (std::size_t nt = num_tests() - 1; nt != no; --nt)
    {
        bool ok = true;

        ok = ok &&
             ::rename(input_file(nt - 1).c_str(), input_file(nt).c_str()) == 0;
        ok = ok && ::rename(solution_file(nt - 1).c_str(),
                            solution_file(nt).c_str()) == 0;

        if (!ok)
        {
            test_cases_.resize(nt - 1);
            return false;
        }

        test_cases_[nt] = test_cases_[nt - 1];
    }

    std::string in_name  = input_file(no);
    std::string sol_name = solution_file(no);

    bool ok = file_content::save_string(in, in_name, 0660) &&
              file_content::save_string(sol, sol_name, 0660);

    if (ok)
    {
        test_cases_[no] = {static_cast<int>(in.size()),
                           static_cast<int>(sol.size())};
    }
    else
    {
        test_cases_.resize(no);
    }

    return ok;
}

bool task::remove_test(std::size_t const no)
{
    std::string in_name  = input_file(no);
    std::string sol_name = solution_file(no);

    if (no + 1 == num_tests())
    {
        ::unlink(in_name.c_str());
        ::unlink(sol_name.c_str());
        test_cases_.pop_back();
        return true;
    }

    if (no > num_tests())
    {
        return false;
    }

    for (std::size_t i = no; i + 1 < num_tests(); ++i)
    {
        ::rename(input_file(i + 1).c_str(), input_file(i).c_str());
        ::rename(solution_file(i + 1).c_str(), solution_file(i).c_str());
        test_cases_[i] = test_cases_[i + 1];
    }
    test_cases_.pop_back();

    return true;
}

bool task::remove_all_tests()
{
    while (test_cases_.size())
    {
        std::string in_name  = input_file(test_cases_.size() - 1);
        std::string sol_name = solution_file(test_cases_.size() - 1);

        auto ok = (::unlink(in_name.c_str()) == 0) &&
                  (::unlink(sol_name.c_str()) == 0);
        test_cases_.pop_back();

        if (!ok)
        {
            return false;
        }
    }

    return true;
}

bool task::set_checker_source(std::string const& code, compiler_type lang,
                              file_content const& bin)
{
    // remove old one if exists
    if (checker_lang_ != compiler_type::NONE)
    {
        std::string to_unlink = compiler_traits::checker_extension(
            path_ + "/" + name_ + ".checker", checker_lang_);
        checker_lang_ = compiler_type::NONE;
        ::unlink(to_unlink.c_str());
    }

    std::string ext = compiler_traits::checker_extension(lang);

    bool ok = file_content::save_string(
                  code, path_ + "/" + name_ + ".checker" + ext, 0660) &&
              bin.save(path_ + "/" + name_ + ".checker", 0770);

    if (ok)
    {
        checker_lang_         = lang;
        has_compiled_checker_ = true;
    }

    return ok;
}

std::pair<bool, std::string>
task::get_test_input_as_string(std::size_t const no) const
{
    return get_test_in_or_sol_as_string(no, ".in");
}

std::pair<bool, std::string>
task::get_test_solution_as_string(std::size_t const no) const
{
    return get_test_in_or_sol_as_string(no, ".sol");
}

std::pair<bool, std::string>
task::get_test_in_or_sol_as_string(std::size_t const  no,
                                   std::string const& ext) const
{
    if (no < num_tests())
    {
        return file_content::load_to_string(test_file(no) + ext);
    }
    return std::pair<bool, std::string>();
}

bool task::lock()
{
    if (is_locked_)
    {
        return true;
    }

    fs_utils::touch_file(lock_file(), "locked\n");

    if (fs_utils::file_exist(lock_file()))
    {
        is_locked_ = true;
        return true;
    }

    return false;
}

bool task::unlock()
{
    if (!is_locked_)
    {
        return true;
    }

    if (fs_utils::remove_file(lock_file()))
    {
        is_locked_ = false;
        return true;
    }

    return false;
}

} // namespace codah
