#include "meta_file.hpp"

#include <fstream>
#include <iostream>
#include <sstream>
#include <stdexcept>

namespace codah
{

meta_file::meta_file(meta_file&& other)
    : map_(std::move(other.map_))
{
}

meta_file& meta_file::operator=(meta_file&& other)
{
    map_ = std::move(other.map_);
    return *this;
}

meta_file::meta_file(std::string const& fname)
{
    std::ifstream fin(fname.c_str(), std::ifstream::in);

    std::string line;

    while (std::getline(fin, line))
    {
        auto p = line.find(':');
        if (p != std::string::npos)
        {
            map_[line.substr(0, p)] = line.substr(p + 1);
        }
        else
        {
            throw std::logic_error("meta file data doesn't contain \":\"");
        }
    }
}

bool meta_file::exists(std::string const& key) const
{
    return map_.count(key) > 0;
}

std::string meta_file::serialize() const
{
    std::ostringstream oss;
    oss << "{";

    int i = 0;
    for (auto const& e : map_)
    {
        if ((i++) > 0)
            oss << "; ";
        oss << e.first << ":" << e.second;
    }
    oss << "}";

    return oss.str();
}

} // namespace codah
