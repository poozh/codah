#pragma once

#include <cassert>
#include <cstddef>
#include <memory>
#include <optional>
#include <string>
#include <unordered_map>
#include <utility>
#include <vector>

namespace codah
{

enum struct compiler_type : int
{
    NONE = 0,
    // C versions
    C_GCC_10_2   = 0x100000,
    C99_GCC_10_2 = 0x101000,

    // C++ versions
    CPP98_GCC_10_2_0 = 0x200000,
    CPP11_GCC_10_2_0 = 0x201000,
    CPP14_GCC_10_2_0 = 0x202000,
    CPP17_GCC_10_2_0 = 0x203000,

    // Pascal
    PASCAL_FPC_3_2_0 = 0x400000,

    // Java
    JAVA_OPENJDK_15 = 0x800000,

    // Python
    PYTHON_2_7 = 0x1000000,
    PYTHON_3_9 = 0x1001000

    // Basic
    // BASIC_FBC_1_07_3 = 0x2000000,
};

struct compiler_traits
{
private:
    static inline std::unordered_map<compiler_type, char const*> const
        checker_extension_map = {
            // C
            {compiler_type::C_GCC_10_2, ".gcc10.2.0.c"},
            {compiler_type::C99_GCC_10_2, ".gcc10.2.0.c99.c"},
            // C++
            {compiler_type::CPP98_GCC_10_2_0, ".gcc10.2.0.cpp98.cpp"},
            {compiler_type::CPP11_GCC_10_2_0, ".gcc10.2.0.cpp11.cpp"},
            {compiler_type::CPP14_GCC_10_2_0, ".gcc10.2.0.cpp14.cpp"},
            {compiler_type::CPP17_GCC_10_2_0, ".gcc10.2.0.cpp17.cpp"},
            // Pascal
            {compiler_type::PASCAL_FPC_3_2_0, ".fpc3.2.0.pas"}};

    static inline std::unordered_map<compiler_type, char const*> const
        extension_map = {
            // C
            {compiler_type::C_GCC_10_2, ".c"},
            {compiler_type::C99_GCC_10_2, ".c"},
            // C++
            {compiler_type::CPP98_GCC_10_2_0, ".cpp"},
            {compiler_type::CPP11_GCC_10_2_0, ".cpp"},
            {compiler_type::CPP14_GCC_10_2_0, ".cpp"},
            {compiler_type::CPP17_GCC_10_2_0, ".cpp"},
            // Pascal
            {compiler_type::PASCAL_FPC_3_2_0, ".pas"},
            // Java
            {compiler_type::JAVA_OPENJDK_15, ".java"},
            // Basic
            // {compiler_type::BASIC_FBC_1_07_3, ".bas"},
            // Python
            {compiler_type::PYTHON_2_7, ".py"},
            {compiler_type::PYTHON_3_9, ".py"}};

    static constexpr int C_LIKE_MASK      = 0x100000;
    static constexpr int CPP_LIKE_MASK    = 0x200000;
    static constexpr int PASCAL_LIKE_MASK = 0x400000;
    static constexpr int JAVA_LIKE_MASK   = 0x800000;
    // static constexpr int BASIC_LIKE_MASK  = 0x500000;
    static constexpr int PYTHON_LIKE_MASK = 0x1000000;

public:
    static std::unordered_map<compiler_type, char const*> const
    get_checker_langs()
    {
        return checker_extension_map;
    }

    static inline bool is_c_like(int lang) { return lang & C_LIKE_MASK; }
    static inline bool is_cpp_like(int lang) { return lang & CPP_LIKE_MASK; }
    static inline bool is_pascal_like(int lang)
    {
        return lang & PASCAL_LIKE_MASK;
    }
    static inline bool is_java_like(int lang) { return lang & JAVA_LIKE_MASK; }
    // static inline bool is_basic_like(int lang)
    // {
    //     return lang & BASIC_LIKE_MASK;
    // }
    static inline bool is_python_like(int lang)
    {
        return lang & PYTHON_LIKE_MASK;
    }

    static inline bool is_c_like(compiler_type lang)
    {
        return is_c_like(static_cast<int>(lang));
    }
    static inline bool is_cpp_like(compiler_type lang)
    {
        return is_cpp_like(static_cast<int>(lang));
    }
    static inline bool is_pascal_like(compiler_type lang)
    {
        return is_pascal_like(static_cast<int>(lang));
    }
    static inline bool is_java_like(compiler_type lang)
    {
        return is_java_like(static_cast<int>(lang));
    }
    // static inline bool is_basic_like(compiler_type lang)
    // {
    //     return is_basic_like(static_cast<int>(lang));
    // }
    static inline bool is_python_like(compiler_type lang)
    {
        return is_python_like(static_cast<int>(lang));
    }

    static inline std::optional<char const*> c_standard_flag(compiler_type lang)
    {
        assert(is_c_like(lang));
        switch (lang)
        {
        case compiler_type::C99_GCC_10_2:
            return "-std=gnu99";
            break;
        default:
            return std::nullopt;
        }
    }

    static inline std::optional<char const*>
    cpp_standard_flag(compiler_type lang)
    {
        assert(is_cpp_like(lang));
        switch (lang)
        {
        case compiler_type::CPP98_GCC_10_2_0:
            return "-std=c++98";
            break;
        case compiler_type::CPP11_GCC_10_2_0:
            return "-std=c++11";
            break;
        case compiler_type::CPP14_GCC_10_2_0:
            return "-std=c++14";
            break;
        case compiler_type::CPP17_GCC_10_2_0:
            return "-std=c++17";
            break;
        default:
            return std::nullopt;
        }
    }

    static bool is_supported_as_checker(compiler_type lang)
    {
        return checker_extension_map.count(lang);
    }

    static bool is_supported(compiler_type lang)
    {
        return extension_map.count(lang);
    }

    static bool is_supported_as_checker(int lang)
    {
        return checker_extension_map.count(static_cast<compiler_type>(lang));
    }

    static bool is_supported(int lang)
    {
        return extension_map.count(static_cast<compiler_type>(lang));
    }

    static char const* checker_extension(compiler_type lang)
    {
        assert(is_supported_as_checker(lang));
        return checker_extension_map.at(lang);
    }

    static char const* extension(compiler_type lang)
    {
        assert(is_supported(lang));
        return extension_map.at(lang);
    }

    static std::string extension(std::string const& base, compiler_type lang)
    {
        return base + extension(lang);
    }

    static std::string checker_extension(std::string const& base,
                                         compiler_type      lang)
    {
        return base + checker_extension(lang);
    }
};

struct grade_status
{
    static constexpr int OK                    = 0;
    static constexpr int COMPILE_ERROR         = 1;
    static constexpr int LANG_NOT_SUPPORTED    = 2;
    static constexpr int PROBLEM_NOT_FOUND     = 3;
    static constexpr int TESTS_MISSING         = 4;
    static constexpr int UNKNOWN_GRADER_ERRROR = 9;
};

struct grader_status
{
    static constexpr int OK                    = 0;
    static constexpr int COMPILE_ERROR         = 1;
    static constexpr int LANG_NOT_SUPPORTED    = 2;
    static constexpr int PROBLEM_NOT_FOUND     = 3;
    static constexpr int TESTS_MISSING         = 4;
    static constexpr int UNKNOWN_GRADER_ERRROR = 9;

    static constexpr int TASK_NOT_FOUND      = 1001;
    static constexpr int TASK_ALREADY_EXISTS = 1002;
    static constexpr int TASK_ALREADY_LOCKED = 1010;
    static constexpr int TASK_NOT_LOCKED     = 1011;

    static constexpr int UNKNOWN_ERROR = 9999999;

    static constexpr int TEST_CASE_MISSING = 4001;
};

struct grade_result
{
    static constexpr int OK     = 0;
    static constexpr int TLE    = 1;
    static constexpr int RTE    = 2;
    static constexpr int MLE    = 3;
    static constexpr int OLE    = 4;
    static constexpr int NOT0   = 5;
    static constexpr int SYSERR = 6;
    static constexpr int NOTEST = 7;

    static constexpr int CHECKER_OK  = 0;
    static constexpr int CHECKER_WA  = 1;
    static constexpr int CHECKER_ERR = 2;
    static constexpr int CHECKER_NA  = 3;

    static constexpr int STATUS_OK                    = 0;
    static constexpr int STATUS_COMPILE_ERROR         = 1;
    static constexpr int STATUS_LANG_NOT_SUPPORTED    = 2;
    static constexpr int STATUS_PROBLEM_NOT_FOUND     = 3;
    static constexpr int STATUS_TESTS_MISSING         = 4;
    static constexpr int STATUS_UNKNOWN_GRADER_ERRROR = 9;

    int status;
    int time;
    int wall_time;
    int memory;
    int solved;
    int test_num;
};

struct grade_report
{
    int                       status;
    std::string               status_message;
    std::vector<grade_result> test_cases;
};

struct test_case_state
{
    int input_size;
    int solution_size;
};

struct task_state
{
    std::string  name;
    bool         is_locked;

    std::int32_t grader_lang;
    bool         has_compiled_grader;

    std::vector<test_case_state> test_cases;
};

} // namespace codah
