#pragma once

#include <memory>
#include <string>
#include <mutex>

namespace codah
{

class executor
{
private:
    using char_ptr = char*;

private:
    std::unique_ptr<char[]> data_;
    char_ptr                head_;
    char_ptr                argv_[128];
    int                     next_idx_;

public:
    executor();

    executor& operator<<(char const*);
    executor& operator<<(std::string const&);

    int operator()();

    static std::mutex fork_mutex;
};

} // namespace codah
