#include "sandbox_group.hpp"

#include <thread>

namespace codah
{

void sandbox_group::box_loop(int id, std::string const& isolate)
{
    sandbox box(id, isolate);

    {
        std::unique_lock<std::mutex> lock(m_);
        ++num_boxes_;
        if (num_boxes_ == total_boxes_)
        {
            master_cv_.notify_one();
        }
    }

    for (;;)
    {
        task_type* task;

        {
            std::unique_lock<std::mutex> lock(m_);
            while (tasks_.size() == 0)
            {
                ++idle_boxes_;
                slave_cv_.wait(lock);
                --idle_boxes_;
            }
            task = tasks_.front();
            tasks_.pop_front();
        }

        if (task == &sentinel_)
        {
            --num_boxes_;
            if (num_boxes_ == 0)
            {
                master_cv_.notify_one();
            }
            return;
        }

        (*task)(&box);
        delete task;
    }
}

sandbox_group::sandbox_group(int total_boxes, std::string const& isolate)
{
    std::unique_lock<std::mutex> lock(m_);

    total_boxes_ = total_boxes;
    for (int i = 0; i < total_boxes; ++i)
    {
        std::thread t(&sandbox_group::box_loop, this, i, isolate);
        t.detach();
    }

    while (num_boxes_ < total_boxes_)
    {
        master_cv_.wait(lock);
    }
}

sandbox_group::~sandbox_group()
{
    std::unique_lock<std::mutex> lock(m_);
    for (int i = 0; i < total_boxes_; ++i)
    {
        tasks_.push_back(&sentinel_);
    }

    slave_cv_.notify_all();

    while (num_boxes_ > 0)
    {
        master_cv_.wait(lock);
    }
}

} // namespace codah
