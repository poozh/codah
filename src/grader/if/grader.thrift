namespace cpp codah.services
namespace php Codah.Grader

struct TestResult
{
  1: i32 status,
  2: i32 time,
  3: i32 wall_time,
  4: i32 memory,
  5: i32 solved,
  6: i32 test_num
}

struct GradeReport
{
  1: i32 status,
  2: string status_message,
  3: list<TestResult> test_cases,
}

struct TestCaseState
{
  1: i32 input_size,
  2: i32 solution_size,
}

struct TaskState
{
  1: string              name,
  2: bool                is_locked,
  3: i32                 grader_lang,
  4: bool                has_compiled_grader,
  5: list<TestCaseState> test_cases,
}

struct CodeFile
{
  1: i32    lang,
  2: binary code,
}

// NEW
exception GraderException {
     1: i32          error_code,
     2: string       message,
}

// NEW
const i32 GRADER_GENERAL_ERROR = 1;


service Grader
{
  GradeReport fullGrade(
      1: binary source,
      2: string task,
      3: i32    language,
      4: i32    time_limit,
      5: i32    memory_limit)
    throws (1: GraderException ge),

  GradeReport partialGrade(
      1: binary source,
      2: string task,
      3: i32    language,
      4: i32    time_limit,
      5: i32    memory_limit,
      6: i32    first_tests,
      7: i32    last_test)
    throws (1: GraderException ge),

  TaskState getTaskState(
      1: string task)
    throws (1: GraderException ge),

  CodeFile getChecker(
      1: string task)
    throws (1: GraderException ge),

  void createTask(
      1: string task)
    throws (1: GraderException ge),

  void addTestCase(
      1: string task,
      2: binary input_file,
      3: binary solution_file)
    throws (1: GraderException ge),

  void removeTestCase(
      1: string task,
      2: i32    test_number)
    throws (1: GraderException ge),

  void removeAllTestCases(
      1: string task)
    throws (1: GraderException ge),

  binary getTestInput(
      1: string task,
      2: i32 nCase)
    throws (1: GraderException ge),

  binary getTestSolution(
      1: string task,
      2: i32 nCase)
    throws (1: GraderException ge),

  void insertTestCase(
      1: string task,
      2: i32    test_number,
      3: binary input_file,
      4: binary solution_file)
    throws (1: GraderException ge),

  void lockTask(
      1: string task)
    throws (1: GraderException ge),

  void unlockTask(
      1: string task)
    throws (1: GraderException ge),

  void setChecker(
      1: string task,
      2: i32 lang,
      3: binary code)
    throws (1: GraderException ge),
}

// Make sure they are the same as ../grader/grader.hpp constants

const i32 STATUS_OK                    = 0
const i32 STATUS_COMPILE_ERROR         = 1
const i32 STATUS_LANG_NOT_SUPPORTED    = 2
const i32 STATUS_PROBLEM_NOT_FOUND     = 3
const i32 STATUS_TESTS_MISSING         = 4
const i32 STATUS_UNKNOWN_GRADER_ERRROR = 9

const i32 GRADER_STATUS_NO_TASK_FOUND = 1001
const i32 GRADER_STATUS_TASK_ALREADY_EXISTS = 1002

const i32 GRADER_STATUS_TASK_ALREADY_LOCKED = 1010
const i32 GRADER_STATUS_TASK_NOT_LOCKED = 1011

const i32 GRADER_STATUS_UNKNOWN_ERROR = 9999999

// Test cases related status
const i32 GRADER_STATUS_TEST_CASE_MISSING = 4001
