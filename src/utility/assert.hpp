#pragma once

#include <glog/logging.h>
#include <sstream>
#include <string>
#include <zi/assert.hpp>
#include <zi/zpp/stringify.hpp>

#define DIE(message)                                                           \
    {                                                                          \
        LOG(FATAL) << message << " "                                           \
                   << "file: " << __FILE__ << " line: " << __LINE__;           \
    }                                                                          \
    static_cast<void>(0)

#define UNIMPLEMENTED()                                                        \
    {                                                                          \
        LOG(FATAL) << "unimplemented function: "                               \
                   << "file: " << __FILE__ << " line: " << __LINE__;           \
    }                                                                          \
    static_cast<void>(0)

#define STRONG_ASSERT(condition)                                               \
    if (!(condition))                                                          \
    {                                                                          \
        LOG(FATAL) << "Assertion " << ZiPP_STRINGIFY(condition) << " failed "  \
                   << "file: " << __FILE__ << " line: " << __LINE__;           \
    }                                                                          \
    static_cast<void>(0)

template <typename F, typename L>
inline std::string ___this_file_this_line(const F& f, const L& l)
{
    std::ostringstream oss;
    oss << "\nfile: " << f << "\nline: " << l << "\n";
    return oss.str();
}

#define HERE() ___this_file_this_line(__FILE__, __LINE__)
