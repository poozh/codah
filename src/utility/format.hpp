#pragma once

#include <algorithm>
#include <boost/lexical_cast.hpp>
#include <cstddef>
#include <string>
#include <utility>

namespace codah
{

inline std::string format_bytes(std::size_t n)
{
    int l = 0;
    n *= 100;
    while (n >= 100000)
    {
        ++l;
        n /= 1024;
        if (l == 4)
        {
            break;
        }
    }

    std::string result = boost::lexical_cast<std::string>(n / 100) + '.' +
                         boost::lexical_cast<std::string>(n % 100);

    switch (l)
    {
    case 0:
        return result + " b";
    case 1:
        return result + " kb";
    case 2:
        return result + " mb";
    case 3:
        return result + " gb";
    case 4:
        return result + " tb";
    default:
        return result;
    }
}

} // namespace codah
