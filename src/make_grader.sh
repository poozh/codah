mkdir -p gen
mkdir -p bin
thrift -I ./ -r --gen cpp -o ./gen ./windsor/if/windsor.thrift
thrift -I ./ -r --gen php -o ./gen ./windsor/if/windsor.thrift
thrift -I ./ -r --gen cpp -o ./gen ./grader/if/grader.thrift
thrift -I ./ -r --gen php -o ./gen ./grader/if/grader.thrift
g++ -g -std=c++2a grader/*.cpp  gen/gen-cpp/Grader.cpp gen/gen-cpp/grader_types.cpp gen/gen-cpp/grader_constants.cpp gen/gen-cpp/windsor_service.cpp  -I./ -I./gen  -I./zi_lib -I/usr/include/thrift -lthrift -lpthread -lgflags -lglog -o bin/grader_server  -DNDEBUG123 -fno-omit-frame-pointer -Wall -Wextra
cp -r ./gen/gen-php/Codah ../test/assets/
