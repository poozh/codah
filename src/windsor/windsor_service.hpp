#pragma once

#include "gen-cpp/windsor_service.h"
#include "utility/format.hpp"
#include <map>
#include <mutex>
#include <string>
#include <zi/system.hpp>
#include <zi/time.hpp>

namespace codah
{

class windsor_service : virtual public windsor_serviceIf
{
private:
    std::string name_;
    int64_t     startup_time_;

    std::map<std::string, std::string> options_;
    std::mutex options_mutex_;

    std::map<std::string, int64_t> counters_;
    std::mutex counters_mutex_;

public:
    windsor_service(const std::string& name)
        : name_(name)
        , startup_time_(zi::now::msecs())
        , options_()
        , options_mutex_()
        , counters_()
        , counters_mutex_()
    {
    }

    void get_name(std::string& _return) { _return = name_; }

    void get_version(std::string& _return) { _return = "beta"; }

    int64_t get_counter(const std::string& key)
    {
        std::unique_lock<std::mutex> g(counters_mutex_);
        std::map<std::string, int64_t>::const_iterator it = counters_.find(key);
        if (it != counters_.end())
        {
            return it->second;
        }
        else
        {
            return 0;
        }
    }

    int64_t add_counter(const std::string& key, const int64_t value)
    {
        std::unique_lock<std::mutex> g(counters_mutex_);
        counters_[key] += value;
    }

    int64_t inc_counter(const std::string& key) { return add_counter(key, 1); }

    void get_counters(std::map<std::string, int64_t>& _return)
    {
        std::unique_lock<std::mutex> g(counters_mutex_);
        _return = counters_;
    }

    void set_option(const std::string& key, const std::string& value)
    {
        std::unique_lock<std::mutex> g(options_mutex_);
        options_[key] = value;
    }

    void get_option(std::string& _return, const std::string& key)
    {
        std::unique_lock<std::mutex> g(options_mutex_);
        std::map<std::string, std::string>::const_iterator it =
            options_.find(key);
        if (it != options_.end())
        {
            _return = it->second;
        }
        else
        {
            _return = "";
        }
    }

    int64_t get_lifetime() { return zi::now::msecs() - startup_time_; }

    void process_info(std::map<std::string, int64_t>& _return)
    {
        _return["CPU count"] = static_cast<int64_t>(zi::system::cpu_count);
        _return["Mem total"] =
            static_cast<int64_t>(zi::system::memory::total());
        _return["Mem available"] =
            static_cast<int64_t>(zi::system::memory::available());
        _return["Mem used"] = static_cast<int64_t>(zi::system::memory::usage());
        _return["Mem used (virt)"] =
            static_cast<int64_t>(zi::system::memory::usage(true));
    }

    void pretty_process_info(std::map<std::string, std::string>& _return)
    {
        _return["CPU count"] = format_bytes(zi::system::cpu_count);
        _return["Mem total"] = format_bytes(zi::system::memory::total());
        _return["Mem available"] =
            format_bytes(zi::system::memory::available());
        _return["Mem used"] = format_bytes(zi::system::memory::usage());
        _return["Mem used (virt)"] =
            format_bytes(zi::system::memory::usage(true));
    }
};

} // namespace codah
