namespace cpp codah
namespace php Codah

service windsor_service
{
    string get_name(),
    string get_version(),
    i64 get_counter(1: string key),
    i64 inc_counter(1: string key),
    i64 add_counter(1: string key, 2:i64 value),
    map<string, i64> get_counters(),
    void set_option(1:string key, 2:string value),
    string get_option(1:string key),
    i64 get_lifetime(),
    map<string, i64> process_info(),
    map<string, string> pretty_process_info(),
}