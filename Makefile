# -*- Makefile -*-

HERE        =   .
EXTERNAL    =   $(HERE)/external
GENDIR      =   $(HERE)/gen
GENERATED_CPP = $(GENDIR)/gen-cpp
GENERATED_PHP = $(GENDIR)/gen-php

AT      =   @

CP      =   $(AT)cp
ECHO    =   $(AT)echo
MKDIR   =   $(AT)mkdir
MV      =   $(AT)mv
RM      =   $(AT)rm
TOUCH   =   $(AT)touch
TAR     =   $(AT)tar
FIND    =   $(AT)find
CXX     =   $(AT)g++
THRIFT  =   $(AT)thrift

# Compile Flags ############################
CXXWARN            =    -Wall -Wextra -Werror
COMMON_CXXFLAGS    =    -g -fPIC $(CXXWARN) -std=c++2a -MMD -MP -MT "$(@)" -MF $(@:.o=.d)

DBG_CXXFLAGS       =    $(COMMON_CXXFLAGS) -Og
OPT_CXXFLAGS       =    $(COMMON_CXXFLAGS) -DNDEBUG -O2 -fno-omit-frame-pointer

ifneq ($(strip $(OPT)),)
  CXXFLAGS  =   $(OPT_CXXFLAGS)
  BUILDDIR  =   ./build/release
  BINDIR    =   ./bin/release
else
  CXXFLAGS  =   $(DBG_CXXFLAGS)
  BUILDDIR  =   ./build/debug
  BINDIR    =   ./bin/debug
endif


# don't delete intermediate files
.SECONDARY:

define build_cpp
	$(ECHO) "[CXX] compiling $<"
	$(MKDIR) -p $(dir $@)
	$(CXX) -c $(CXXFLAGS) $1 -o $@ $<
endef

define link_cpp
	$(ECHO) "[CXX] linking $@"
	$(MKDIR) -p $(dir $@)
	$(CXX) $(CXXFLAGS) -o $@ $^ $1
endef


define lib_deps
	$(eval $1_SOURCES = $(shell find lib/$2 -iname "*.cpp" 2>/dev/null))
	$(eval $1_MAIN = $(BUILDDIR)/exe/$2.o)
	$(eval $1_DEPS = $(subst lib/$2,$(BUILDDIR)/lib/$2,$3 $($1_SOURCES:.cpp=.o)))
endef

define thrift_deps
	$(eval $1_SOURCES = $(shell find gen/$2/gen-cpp -iname "*.cpp" 2>/dev/null))
	$(eval $1_DEPS = $(subst gen/$2/gen-cpp,$(BUILDDIR)/generated/$2/gen-cpp,$3 $($1_SOURCES:.cpp=.o)))
	$(eval $1_INCLUDES = -I./gen/$2/gen-cpp)
endef

THRIFT_FILES = $(shell find thrift/if -iname "*.thrift" 2>/dev/null)
THRIFT_PROXY_DEPS = $(patsubst thrift/if/%.thrift,$(GENDIR)/%.thrift_proxy,$(THRIFT_FILES))

$(GENDIR)/%.thrift_proxy: thrift/if/%.thrift
	$(ECHO) "[Thrift] Generating $@"
	$(MKDIR) -p $(dir $@)/$(basename $(notdir $@))
	$(TOUCH) $@.tmp
	$(THRIFT) -I ./thrift/if -r --gen cpp -o $(GENDIR)/$(basename $(notdir $@)) $<
	$(RM) $(GENDIR)/$(basename $(notdir $@))/gen-cpp/*.skeleton.cpp
	$(THRIFT) -I ./thrift/if -r --gen php -o $(GENDIR)/$(basename $(notdir $@)) $<
	$(MV) $@.tmp $@

## GRADER
$(eval $(call lib_deps,GRADER,grader))
$(eval $(call thrift_deps,THRIFT_GRADER,grader))

$(BUILDDIR)/generated/grader/%.o: gen/grader/%.cpp $(THRIFT_PROXY_DEPS)
	$(call build_cpp, $(THRIFT_GRADER_INCLUDES))

$(BUILDDIR)/lib/%.o: lib/%.cpp
	$(call build_cpp, -I./include)

$(BINDIR)/grader: $(HERE)/exe/grader.cpp $(THRIFT_GRADER_DEPS) $(GRADER_DEPS)
	$(call link_cpp,$(THRIFT_GRADER_INCLUDES) -I./include -I./lib -lthrift -lpthread -lgflags -lglog)

.PHONY: clean
clean:
	$(ECHO) Cleaning...
	$(RM) -rf bin build gen



.PHONY: all
all: $(THRIFT_PROXY_DEPS) $(BINDIR)/grader
	$(ECHO) $(THRIFT_GRADER_INCLUDES)
	$(ECHO) $(THRIFT_GRADER_DEPS)

# Automatic dependencies ####################################
ALLDEPS = $(shell find $(BUILDDIR) -iname "*.d" 2>/dev/null)
-include $(ALLDEPS)
