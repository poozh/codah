#include "fs_utils.hpp"

#include <cstring>
#include <dirent.h>

namespace codah
{
namespace fs_utils
{

std::list<std::string> ls_dirs(std::string const& path)
{
    std::list<std::string> ret;

    DIR* dp = ::opendir(path.c_str());
    if (dp == 0)
    {
        return ret;
    }

    struct dirent* dirp;

    while ((dirp = ::readdir(dp)) != nullptr)
    {
        if ((::strcmp(dirp->d_name, ".") != 0) &&
            (::strcmp(dirp->d_name, "..") != 0))
        {
            std::string full_path = path + "/" + dirp->d_name;
            struct stat status;
            if (::stat(full_path.c_str(), &status) == 0 &&
                S_ISDIR(status.st_mode))
            {
                ret.push_back(std::string(dirp->d_name));
            }
        }
    }

    (void)::closedir(dp);
    return ret;
}
} // namespace fs_utils
} // namespace codah
