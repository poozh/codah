#include "executor.hpp"

#include <iostream>
#include <stdexcept>
#include <sys/wait.h>
#include <system_error>
#include <unistd.h>

namespace codah
{

std::mutex executor::fork_mutex;

executor::executor()
    : data_(std::make_unique<char[]>(4096))
    , head_(data_.get())
    , argv_()
    , next_idx_(0)
{
}

executor& executor::operator<<(char const* s)
{
    argv_[next_idx_] = head_;
    int n            = ::sprintf(head_, "%s", s);
    head_[n]         = '\0';
    head_ += n + 1;
    ++next_idx_;
    return *this;
}

executor& executor::operator<<(std::string const& s)
{
    argv_[next_idx_] = head_;
    int n            = ::sprintf(head_, "%s", s.c_str());
    head_[n]         = '\0';
    head_ += n + 1;
    ++next_idx_;
    return *this;
}

int executor::operator()()
{
    argv_[next_idx_] = 0;

    pid_t pid;
    {
        std::unique_lock<std::mutex> lock(fork_mutex);
        ::fflush(nullptr);
        pid = fork();
    }

    if (pid < 0)
    {
        throw std::system_error(errno, std::system_category());
    }

    if (pid > 0)
    {
        int  status;
        auto w = waitpid(pid, &status, 0);

        if (w == -1)
        {
            throw std::system_error(errno, std::system_category());
        }
        else
        {
            return status;
        }
    }
    else
    {
        ::execve(argv_[0], argv_, environ);
        _exit(1);
    }
}

} // namespace codah
