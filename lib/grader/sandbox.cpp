#include "sandbox.hpp"

#include "executor.hpp"
#include "meta_file.hpp"

#include <cstdio>
#include <gflags/gflags.h>
#include <glog/logging.h>
#include <iostream>

DECLARE_string(sandboxes_root);

namespace codah
{

sandbox::sandbox(int id, std::string const& isolate)
    : id_(id)
    , path_(FLAGS_sandboxes_root + "/" + std::to_string(id) +
            std::string("/box/"))
    , isolate_(isolate)
    , meta_file_(FLAGS_sandboxes_root + "_meta_file_" + std::to_string(id))
{
}

bool sandbox::cleanup() const
{

    // cleanup
    {
        executor e;
        e << isolate_ << "-b" << std::to_string(id_) << "--cg"
          << "--cleanup";

        e();

        // Cleanup can fail for now (happens when the grader is ran
        // for the first time
        //
        // if (!WIFEXITED(status) || WEXITSTATUS(status) != 0)
        // {
        //     throw std::runtime_error("isolate --cleanup failed");
        // }
    }

    // prepare
    {
        executor e;
        e << isolate_ << "-b" << std::to_string(id_) << "--cg"
          << "--init";

        auto status = e();

        if (!WIFEXITED(status) || WEXITSTATUS(status) != 0)
        {
            throw std::runtime_error("isolate --prepare failed");
        }
    }

    return true;
}

std::pair<bool, file_content> sandbox::compile(std::string const& code,
                                               compiler_type      type) const
{
    assert(compiler_traits::is_supported(type));

    if (compiler_traits::is_python_like(type))
    {
        return {true, file_content(code, file_content::from_string())};
    }

    std::pair<bool, file_content> ret;

    std::string code_file_name = compiler_traits::extension("zi", type);

    cleanup();

    file_content::save_string(code, path_ + code_file_name, 0400);

    // finally - the compile command
    executor e;

    e << isolate_ << "-b" << std::to_string(id_) << "--cg"
      << "--cg-timing"
      << "--cg-mem=1048576"
      << "--fsize=20480"
      << "--env=PATH"
      << "-t"
      << "15000"
      << "-w"
      << "60000"
      << "-x"
      << "15000"
      << "-k"
      << "0"
      << "-r"
      << "stderr.txt"
      << "-i"
      << "/dev/null"
      << "-o"
      << "stdout.txt"
      << "--processes=20"
      //<< "-v"
      << "-M" << meta_file_ << "--run"
      << "--";

    if (compiler_traits::is_cpp_like(type))
    {
        e << "/usr/bin/g++";

        auto standard_flag = compiler_traits::cpp_standard_flag(type);

        if (standard_flag)
        {
            e << *standard_flag;
        }

        e << "zi.cpp"
          << "-O2"
          << "-o"
          << "zi"
          << "-lm"
          << "-static";
    }
    else if (compiler_traits::is_c_like(type))
    {
        e << "/usr/bin/gcc";

        auto standard_flag = compiler_traits::c_standard_flag(type);

        if (standard_flag)
        {
            e << *standard_flag;
        }

        e << "zi.c"
          << "-O2"
          << "-o"
          << "zi"
          << "-lm"
          << "-static";
    }
    else if (compiler_traits::is_pascal_like(type))
    {
        e << "/usr/bin/fpc"
          << "zi.pas"
          << "-So"
          << "-O2"
          << "-XS"
          << "-ozi";
    }
    else if (compiler_traits::is_java_like(type))
    {
        e << "/usr/bin/javac"
          << "zi.java";
    }

    auto status = e();

    std::string bin_file = path_ + "zi";
    if (compiler_traits::is_java_like(type))
    {
        bin_file += ".class";
    }

    // TODO: deal with pascal errors that are in stdout
    if (WIFEXITED(status) && (WEXITSTATUS(status) == 0))
    {
        file_content fc(bin_file);
        if (fc.good())
        {
            ret.first  = true;
            ret.second = std::move(fc);
            return ret;
        }
    }

    ret.first = false;

    if (compiler_traits::is_pascal_like(type))
    {
        ret.second = file_content(path_ + "stdout.txt");
    }
    else
    {
        ret.second = file_content(path_ + "stderr.txt");
    }

    return ret;
}

grade_result sandbox::grade(file_content const& bin, compiler_type lang,
                            file_content const& fin, file_content const& fsol,
                            file_content const& fgrader,
                            std::size_t const   time_lim,
                            std::size_t const   mem_lim,
                            std::size_t const   out_lim) const
{

    std::cout << "Grade start\n";
    cleanup();
    {
        std::unique_lock<std::mutex> lock(executor::fork_mutex);
        if (compiler_traits::is_java_like(lang))
        {
            bin.save(path_ + "zi.class", 0400);
        }
        else
        {
            bin.save(path_ + "zi", 0500);
        }
    }
    fin.save(path_ + "stdin.txt", 0400);

    std::cout << "Grade clean\n";

    executor e;

    e << isolate_ << "-b" << std::to_string(id_) << "--cg"
      << "--cg-timing"
      << "--cg-mem=" + std::to_string(2 * 1024 * 1024)
      << "--fsize=" + std::to_string(out_lim) << "--env=PATH"
      << "-t" << std::to_string(time_lim) << "-w"
      << std::to_string(time_lim + 100000) << "-x"
      << std::to_string(time_lim + 100) << "-k"
      << "0"
      << "-r"
      << "stderr.txt"
      << "-i"
      << "stdin.txt"
      << "-o"
      << "stdout.txt";

    if (compiler_traits::is_java_like(lang))
    {
        e << "--processes=40";
    }
    else if (compiler_traits::is_python_like(lang))
    {
        e << "--processes=4";
    }
    else
    {
        e << "--processes=1";
    }

    // e << "-v"
    e << "-M" << meta_file_ << "--run"
      << "--";

    if (compiler_traits::is_java_like(lang))
    {
        e << "/usr/bin/java"
          << "-XX:MaxRAM=1g"
          << "zi";
    }
    else if (compiler_traits::is_python_like(lang))
    {
        e << "/usr/bin/python3.9"
          << "zi";
    }
    else
    {
        e << "./zi";
    }

    auto status = e();

    meta_file mf(meta_file_);

    int time      = static_cast<int>(mf.value_as<double>("time") * 1000);
    int wall_time = static_cast<int>(mf.value_as<double>("time-wall") * 1000);
    int memory    = mf.value_as<int>("cg-mem");

    if (static_cast<std::size_t>(time) > time_lim ||
        wall_time > (time * 2 + 3000))
    {
        return {grade_result::TLE, 0, 0, 0, grade_result::CHECKER_NA, -1};
    }

    if (mf.value_as<std::size_t>("cg-mem") >= mem_lim)
    {
        return {grade_result::MLE, 0, 0, 0, grade_result::CHECKER_NA, -1};
    }

    if (WIFEXITED(status) && WEXITSTATUS(status) == 0)
    {

        std::cout << "Exec ok\n";

        {
            std::unique_lock<std::mutex> lock(executor::fork_mutex);
            fgrader.save(path_ + "checker", 0500);
        }
        fsol.save(path_ + "sol.txt", 0400);

        executor ce;

        std::cout << "Grader start" << std::endl;

        ce << isolate_ << "-b" << std::to_string(id_) << "--cg"
           << "--cg-timing"
           << "--cg-mem=" + std::to_string(1024 * 1024)
           << "--fsize=" + std::to_string(128 * 1024) << "--env=PATH"
           << "-t" << std::to_string(2000) << "-w" << std::to_string(10000)
           << "-x" << std::to_string(2000) << "-k"
           << "0"
           << "-r"
           << "cstderr.txt"
           << "-i"
           << "/dev/null"
           << "-o"
           << "cstdout.txt"
           << "--processes=1"
           //<< "-v"
           << "-M" << meta_file_ << "--run"
           << "--"
           << "./checker"
           << "stdin.txt"
           << "stdout.txt"
           << "sol.txt";

        std::cout << "Grader end" << std::endl;

        int cstatus = ce();

        meta_file cmf(meta_file_);

        if (WIFEXITED(cstatus) && WEXITSTATUS(cstatus) == 0)
        {

            int score = 0;

            std::string p = path_ + "cstdout.txt";
            auto        f = std::fopen(p.c_str(), "r");
            if (f)
            {
                std::fscanf(f, "%d", &score);
                std::fclose(f);
            }

            return {grade_result::OK,
                    time,
                    wall_time,
                    memory,
                    (score > 0) ? grade_result::CHECKER_OK
                                : grade_result::CHECKER_WA,
                    -1};
        }
        else
        {
            LOG(ERROR) << "CHECKER ERROR: " << cmf.serialize();
            google::FlushLogFiles(google::ERROR);

            return {grade_result::OK,          time, wall_time, memory,
                    grade_result::CHECKER_ERR, -1};
        }
    }
    else
    {
        auto s = mf.value_as<std::string>("status");

        if (s == "RE")
        {
            return {grade_result::NOT0, 0, 0, 0, grade_result::CHECKER_NA, -1};
        }
        else if (s == "SG")
        {
            return {grade_result::RTE, 0, 0, 0, grade_result::CHECKER_NA, -1};
        }
        else
        {
            LOG(ERROR) << "SYSERR: " << mf.serialize();
            google::FlushLogFiles(google::ERROR);
            return {grade_result::SYSERR,     0, 0, 0,
                    grade_result::CHECKER_NA, -1};
        }
    }
}

} // namespace codah
