#pragma once

#include "grader/grader.hpp"

#include "file_content.hpp"

namespace codah
{

class sandbox
{
private:
    int         id_;
    std::string path_;
    std::string isolate_;
    std::string meta_file_;

public:
    sandbox(int /*id*/, std::string const& /*isolate*/);
    sandbox() {}

    sandbox(sandbox const&) = delete;
    sandbox& operator=(sandbox const&) = delete;

    sandbox(sandbox&& other) { *this = std::move(other); }
    sandbox& operator=(sandbox&& other)
    {
        id_        = other.id_;
        path_      = other.path_;
        isolate_   = other.isolate_;
        meta_file_ = other.meta_file_;
        return *this;
    }

private:
    bool cleanup() const;

public:
    std::pair<bool, file_content> get_binary() const;

    std::pair<bool, file_content> compile(std::string const&,
                                          compiler_type) const;

    grade_result grade(file_content const& bin, compiler_type type,
                       file_content const& fin, file_content const& fsol,
                       file_content const& fgrader, std::size_t const time_lim,
                       std::size_t const mem_lim,
                       std::size_t const out_lim) const;
};

} // namespace codah
