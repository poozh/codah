#pragma once

#include <cassert>
#include <condition_variable>
#include <mutex>

namespace codah
{
class baton
{
private:
    bool                    done_    = false;
    bool                    waiting_ = false;
    std::mutex              m_;
    std::condition_variable cv_;

    baton(baton const&) = delete;
    baton& operator=(baton const&) = delete;
    baton(baton&&)                 = delete;
    baton& operator=(baton&&) = delete;

public:
    baton() {}

    void notify()
    {
        std::unique_lock<std::mutex> l(m_);
        assert(!done_);
        done_ = true;
        cv_.notify_one();
    }

    void wait()
    {
        std::unique_lock<std::mutex> l(m_);
        assert(!waiting_);
        waiting_ = true;
        while (!done_)
        {
            cv_.wait(l);
        }
    }
};
} // namespace codah
