#pragma once

#include <string>
#include <utility>

namespace codah
{

class file_content
{
private:
    ssize_t len_  = -1;
    void*   data_ = nullptr;

public:
    struct from_string
    {
    };

    file_content() {}
    explicit file_content(char const*);
    explicit file_content(std::string const&);

    ~file_content();

    file_content(file_content const&) = delete;
    file_content& operator=(file_content const&) = delete;

    file_content(file_content&&);
    file_content& operator=(file_content&&);
    file_content(std::string const&, from_string);

    ssize_t     size() const;
    bool        good() const;
    std::string as_string(std::size_t n = 1000000000) const;

    static bool save_string(std::string const& /*str*/, char const* /*fname*/,
                            mode_t);

    static bool save_string(std::string const& /*str*/,
                            std::string const& /*fname*/, mode_t);

    static std::pair<bool, std::string> load_to_string(char const* /*fname*/);

    static std::pair<bool, std::string>
    load_to_string(std::string const& /*fname*/);

    bool save(char const*, mode_t) const;
    bool save(std::string const&, mode_t) const;
};

} // namespace codah
