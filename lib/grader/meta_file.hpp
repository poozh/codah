#pragma once

#include <boost/lexical_cast.hpp>
#include <map>
#include <string>

namespace codah
{
class meta_file
{
private:
    std::map<std::string, std::string> map_;

public:
    meta_file(meta_file const&) = delete;
    meta_file& operator=(meta_file const&) = delete;

    meta_file(meta_file&&);
    meta_file& operator=(meta_file&&);

    explicit meta_file(std::string const&);

    bool exists(std::string const&) const;

    template <class T>
    T value_as(std::string const& key) const
    {
        auto i = map_.find(key);
        if (i != map_.end())
        {
            return boost::lexical_cast<T>(i->second);
        }
        else
        {
            return T();
        }
    }

    std::string serialize() const;

}; // class meta_file

} // namespace codah
