#pragma once

#include "grader/grader.hpp"

#include "baton.hpp"
#include "sandbox_group.hpp"
#include "task.hpp"

#include <atomic>

namespace codah
{

class submission
{
private:
    sandbox_group&      boxes_;
    task&               task_;
    std::string const&  code_;
    compiler_type const lang_ = compiler_type::NONE;

    std::size_t const time_limit_;
    std::size_t const memory_limit_;
    std::size_t const out_limit_;

    baton baton_;

    std::pair<bool, file_content> compile_result_;

    std::size_t const first_test_;
    std::size_t const last_test_;

    std::atomic<std::size_t> tests_done_{0};

    file_content checker_;

    grade_report report_;

private:
    void do_test(sandbox const* box, std::size_t i);
    void do_compile(sandbox const* box);

public:
    submission(sandbox_group& boxes, task& t, std::string const& code,
               compiler_type lang, std::size_t const time_limit,
               std::size_t const memory_limit, std::size_t const out_limit,
               std::size_t const first_test, std::size_t const last_test);

    grade_report const& get_report() const { return report_; }
};

} // namespace codah
