#include "submission.hpp"

#include <glog/logging.h>
#include <iostream>

namespace codah
{

void submission::do_test(sandbox const* box, std::size_t i)
{
    assert(i >= first_test_ && i <= last_test_);

    if (task_.has_test(i))
    {
        auto in  = task_.get_test_input(i);
        auto sol = task_.get_test_solution(i);

        auto r = box->grade(compile_result_.second, lang_, in, sol, checker_,
                            time_limit_, memory_limit_, out_limit_);

        r.test_num                          = static_cast<int>(i);
        report_.test_cases[i - first_test_] = r;
    }
    else
    {
        report_.test_cases[i - first_test_] = grade_result{
            grade_result::NOTEST, 0, 0, 0, grade_result::CHECKER_NA,
            static_cast<int>(i)};
    }

    if (++tests_done_ + first_test_ == last_test_ + 1)
    {
        baton_.notify();
    }
}

void submission::do_compile(sandbox const* box)
{
    compile_result_ = box->compile(code_, lang_);

    if (compile_result_.first)
    {
        report_.status = grade_result::STATUS_OK;
        report_.test_cases.resize(last_test_ - first_test_ + 1);

        checker_ = task_.get_checker();
        for (std::size_t i = first_test_; i <= last_test_; ++i)
        {
            boxes_ << [this, i](sandbox const* b) { this->do_test(b, i); };
        }
    }
    else
    {
        report_.status         = grade_result::STATUS_COMPILE_ERROR;
        report_.status_message = compile_result_.second.as_string(1024);

        LOG(INFO) << "Compile error: " << report_.status_message << " <<<";

        baton_.notify();
    }
}

submission::submission(sandbox_group& boxes, task& t, std::string const& code,
                       compiler_type lang, std::size_t const time_limit,
                       std::size_t const memory_limit,
                       std::size_t const out_limit,
                       std::size_t const first_test,
                       std::size_t const last_test)
    : boxes_(boxes)
    , task_(t)
    , code_(code)
    , lang_(lang)
    , time_limit_(time_limit)
    , memory_limit_(memory_limit)
    , out_limit_(out_limit)
    , first_test_(first_test)
    , last_test_(last_test)
{
    assert(last_test_ >= first_test_);

    boxes << [this](sandbox const* box) { this->do_compile(box); };

    baton_.wait();
}

} // namespace codah
