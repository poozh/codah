#include "file_content.hpp"

#include <cstring>
#include <fcntl.h>
#include <stdexcept>
#include <sys/stat.h>
#include <unistd.h>

namespace codah
{

file_content::~file_content()
{
    if (len_ > 0)
    {
        std::free(data_);
    }
}

file_content::file_content(char const* fname)
{
    struct stat status;
    if (::stat(fname, &status) == 0 && S_ISREG(status.st_mode))
    {
        len_   = status.st_size;
        int fd = ::open(fname, O_RDONLY);

        if (len_ > 0 && fd != -1)
        {
            data_     = std::malloc(len_);
            ssize_t n = ::read(fd, data_, len_);
            if (n != len_)
            {
                std::free(data_);
                throw std::length_error(
                    std::string("Error reading from file ") + fname);
            }
        }
        ::close(fd);
    }
}

file_content::file_content(std::string const& fname)
    : file_content(fname.c_str())
{
}

file_content::file_content(file_content&& other)
    : len_(other.len_)
    , data_(other.data_)
{
    other.len_  = -1;
    other.data_ = nullptr;
}

file_content& file_content::operator=(file_content&& other)
{
    len_        = other.len_;
    other.len_  = -1;
    data_       = other.data_;
    other.data_ = nullptr;
    return *this;
}

file_content::file_content(std::string const& data, file_content::from_string)
{
    len_ = data.size();
    if (len_ > 0)
    {
        data_ = std::malloc(len_);
        std::memcpy(data_, data.c_str(), len_);
    }
}

ssize_t file_content::size() const { return len_; }

bool file_content::good() const { return len_ >= 0; }

std::string file_content::as_string(std::size_t n) const
{
    if (len_ > 0)
    {
        return std::string(reinterpret_cast<char*>(data_),
                           std::min(n, static_cast<std::size_t>(len_)));
    }
    return std::string();
}

bool file_content::save_string(std::string const& str, char const* fname,
                               mode_t mode = 0644)
{
    int fd = ::open(fname, O_WRONLY | O_CREAT | O_TRUNC | O_CLOEXEC, mode);

    if (fd == -1)
    {
        return false;
    }

    if (::write(fd, str.data(), str.size()) != static_cast<ssize_t>(str.size()))
    {
        ::close(fd);
        return false;
    }

    ::close(fd);
    return true;
}

bool file_content::save_string(std::string const& str, std::string const& fname,
                               mode_t mode = 0644)
{
    return file_content::save_string(str, fname.c_str(), mode);
}

std::pair<bool, std::string> file_content::load_to_string(char const* fname)
{
    std::pair<bool, std::string> ret;
    ret.first = false;

    struct stat status;
    if (::stat(fname, &status) == 0 && S_ISREG(status.st_mode))
    {
        auto len = status.st_size;
        int  fd  = ::open(fname, O_RDONLY);

        if (fd)
        {
            if (len == 0)
            {
                ret.first = true;
            }
            else if (len > 0)
            {
                ret.second.resize(len);
                ret.first =
                    ::read(fd, reinterpret_cast<void*>(
                                   const_cast<char*>(ret.second.data())),
                           len) == len;
            }
            ::close(fd);
        }
    }

    return ret;
}

std::pair<bool, std::string>
file_content::load_to_string(std::string const& fname)
{
    return load_to_string(fname.c_str());
}

bool file_content::save(char const* fname, mode_t mode = 0644) const
{
    if (len_ < 0)
    {
        return false;
    }

    // This is tricky... this file descriptor might be forked into another
    // process if fork in another thread happens after open and before close
    // this...
    int fd = ::open(fname, O_WRONLY | O_CREAT | O_TRUNC | O_CLOEXEC, mode);

    if (fd == -1)
    {
        return false;
    }

    if (::write(fd, data_, len_) != len_)
    {
        ::close(fd);
        return false;
    }

    ::close(fd);
    return true;
}

bool file_content::save(std::string const& fname, mode_t mode) const
{
    return save(fname.c_str(), mode);
}

} // namespace codah
