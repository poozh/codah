#pragma once

#include "sandbox_group.hpp"
#include "submission.hpp"
#include "task.hpp"

#include <cstddef>
#include <map>
#include <memory>
#include <mutex>
#include <optional>
#include <shared_mutex>
#include <string>
#include <tuple>
#include <utility>

namespace codah
{

class grader_impl
{
private:
    std::map<std::string, std::unique_ptr<task>> tasks_;
    sandbox_group                                boxes_;
    std::mutex                                   checker_setting_mutex_;

    mutable std::shared_mutex map_mutex_;
    using shared_lock = std::shared_lock<std::shared_mutex>;
    using unique_lock = std::unique_lock<std::shared_mutex>;

    std::optional<std::string>
    set_checker_no_lock(task* t, std::string const& code, compiler_type lang);

    task* find_task(std::string const& task_name);

    std::string const& path_;

    grade_report partial_grade_no_lock(task* t, std::string const& code,
                                       compiler_type     lang,
                                       std::size_t const time_limit,
                                       std::size_t const memory_limit,
                                       std::size_t const first_test,
                                       std::size_t const last_test);

public:
    explicit grader_impl(std::string const& path, std::size_t const num_boxes,
                         std::string const& isolate_path,
                         bool               recompile_checkers);

    std::string const& get_path() const { return path_; }
    std::string        task_path(std::string const& task_name) const
    {
        return path_ + '/' + task_name;
    }

    std::pair<std::optional<std::string>, grade_report>
    full_grade(std::string const& task_name, std::string const& code,
               compiler_type lang, std::size_t const time_limit,
               std::size_t const memory_limit);

    std::pair<std::optional<std::string>, grade_report>
    partial_grade(std::string const& task_name, std::string const& code,
                  compiler_type lang, std::size_t const time_limit,
                  std::size_t const memory_limit, std::size_t const first_test,
                  std::size_t const last_test);

    std::pair<bool, std::string> get_test_input(std::string const& task_name,
                                                std::size_t const  no) const;

    std::pair<bool, std::string> get_test_solution(std::string const& task_name,
                                                   std::size_t const  no) const;

    std::optional<std::string> set_checker(std::string const& task_name,
                                           std::string const& code,
                                           compiler_type      lang);

    std::pair<std::optional<std::string>, task_state>
    get_task_state(std::string const& task_name);

    std::tuple<std::optional<std::string>, compiler_type, std::string>
    get_checker(std::string const& task_name);

    std::optional<std::string> lock_task(std::string const& task_name);
    std::optional<std::string> unlock_task(std::string const& task_name);

    std::optional<std::string> create_task(std::string const& task_name);

    std::optional<std::string> add_test(std::string const& task_name,
                                        std::string const& in_data,
                                        std::string const& sol_data);

    std::optional<std::string> insert_test(std::string const& task_name,
                                           std::size_t        no,
                                           std::string const& in_data,
                                           std::string const& sol_data);

    std::optional<std::string> remove_test(std::string const& task_name,
                                           std::size_t        no);

    std::optional<std::string> remove_all_tests(std::string const& task_name);
};

} // namespace codah
