#pragma once

#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <fstream>
#include <list>
#include <optional>
#include <string>
#include <sys/stat.h>
#include <sys/types.h>

namespace codah
{
namespace fs_utils
{

inline bool file_exist(char const* const);
inline bool file_exist(std::string const&);

std::list<std::string> ls_dirs(std::string const&);

// inline implementations

inline bool file_exist(char const* const fname)
{
    struct stat status;
    return ::stat(fname, &status) == 0 && S_ISREG(status.st_mode);
}

inline bool file_exist(std::string const& fname)
{
    return file_exist(fname.c_str());
}

inline void touch_file(std::string const& fname,
                       std::string const& content = "")
{
    std::ofstream fout(fname.c_str());
    fout << content;
}

inline std::optional<std::size_t> get_file_size(std::string const& filename)
{
    struct stat stat_buf;
    int         rc = ::stat(filename.c_str(), &stat_buf);
    return rc == 0 ? std::optional<std::size_t>(
                         static_cast<std::size_t>(stat_buf.st_size))
                   : std::nullopt;
}

inline bool remove_file(std::string const& filename)
{
    return ::remove(filename.c_str()) != 0;
}

} // namespace fs_utils
} // namespace codah
